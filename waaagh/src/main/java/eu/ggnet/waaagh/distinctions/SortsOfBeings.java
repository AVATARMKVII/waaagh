package eu.ggnet.waaagh.distinctions;

/**
 * Contains an enum to differentiate between various instances of Being. Provides
 * each Being with a colour and a symbol for visualization.
 *
 * @author mirko.schulze
 */
public enum SortsOfBeings {

    ORK(Colours.ANSI_GREEN, 'O'),
    HERO(Colours.ANSI_RED, 'H');

    private final String colour;
    private final char badge;

    private SortsOfBeings(String colour, char badge) {
        this.colour = colour;
        this.badge = badge;
    }

    public String getColour() {
        return colour;
    }

    public char getBadge() {
        return badge;
    }

}
