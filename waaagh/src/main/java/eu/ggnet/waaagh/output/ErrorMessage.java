package eu.ggnet.waaagh.output;

/**
 * Contains a static method to indicate an exception.
 *
 * @author mirko.schulze
 */
public class ErrorMessage {

    /**
     * Private constructor to prevent an instantiation.
     */
    private ErrorMessage() {

    }

    /**
     * Uses a pop-up to indicate an exception.
     *
     * @param string1 The first message in the pop-up.
     * @param string2 The second message in the pop-up.
     */
    public static void errorMessage(String string1, String string2) {
        javax.swing.JOptionPane.showMessageDialog(null, string1, string2, javax.swing.JOptionPane.ERROR_MESSAGE);
    }

}
