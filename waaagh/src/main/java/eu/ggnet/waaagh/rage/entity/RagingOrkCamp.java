package eu.ggnet.waaagh.rage.entity;

import eu.ggnet.waaagh.rage.distinctions.OrkSquads;
import eu.ggnet.waaagh.rage.output.OutputRagingNarrator;
import java.util.ArrayList;
import java.util.Collections;
import eu.ggnet.waaagh.rage.output.OutputRagingOrk;
import eu.ggnet.waaagh.rage.output.OutputRagingOrkCamp;
import eu.ggnet.waaagh.rage.output.OutputRagingOrkPack;
import eu.ggnet.waaagh.rage.tools.RagingUtilities;
import eu.ggnet.waaagh.tools.NamesGeneratorOrkCamp;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * RagingOrkCamp extends OrkCamp: specialized to tell a certain story; used for
 * The Rage.
 *
 * @author mirko.schulze
 */
public class RagingOrkCamp {

    private final String name;
    private final ArrayList<RagingOrkPack> ragingPacks;
    private final RagingOrkCampground ragingCampground;
    private final RagingOrkPack kommandoz;
    private final RagingOrkPack moshaz;
    private final RagingOrkPack ballaz;
    private RagingOrkPack victoriousPack;

    /**
     * Constructor to fully initialize an OrkCamp instance that is on the brink
     * of rebellion.
     */
    public RagingOrkCamp() {
        this.name = NamesGeneratorOrkCamp.createName();
        this.ragingPacks = new ArrayList<>();
        this.kommandoz = new RagingOrkPack(OrkSquads.KOMMANDOZ);
        this.moshaz = new RagingOrkPack(OrkSquads.MOSHAZ);
        this.ballaz = new RagingOrkPack(OrkSquads.BALLAZ);
        Collections.addAll(ragingPacks, this.kommandoz, this.moshaz, this.ballaz);
        this.ragingCampground = new RagingOrkCampground();
        this.ragingCampground.submitAndDeletePositionsCamp(this);
    }

    public RagingOrkPack getKommandoz() {
        return kommandoz;
    }

    public RagingOrkPack getMoshaz() {
        return moshaz;
    }

    public RagingOrkPack getBallaz() {
        return ballaz;
    }

    public RagingOrkPack getVictoriousPack() {
        return victoriousPack;
    }

    public String getName() {
        return name;
    }

    public ArrayList<RagingOrkPack> getRagingPacks() {
        return ragingPacks;
    }

    public RagingOrkCampground getRagingCampground() {
        return ragingCampground;
    }

    public void setVictoriousPack(RagingOrkPack victoriousPack) {
        this.victoriousPack = victoriousPack;
    }

    @Override
    public String toString() {
        return "RagingOrkCamp{" + "name=" + name
                + ", ragingPacks=" + ragingPacks
                + ", ragingCampground=" + ragingCampground
                + ", kommandoz=" + kommandoz + ", moshaz=" + moshaz
                + ", ballaz=" + ballaz + ", victoriousPack=" + victoriousPack + '}';
    }

    /**
     * Act I:
     *
     * A for-loop represents parts of the passing day. Each day can have a
     * peaceful outcome or includes a member of the high-ranking Kommandoz
     * RagingOrkPack to pick on one of the other instances of RagingOrkPack in
     * this RagingOrkCamp. An RagingOrkPack that gets picked on, increases its
     * anger. When a specified lever of anger is reached, the weaker
     * RagingOrkPacks join their forces to revolt against their opressors. This
     * leads to act II. If not enough anger is gained, the story ends with a
     * party.
     */
    public void startTheRage() {
        this.ragingCampground.displayPlayground(this);
        int counter = 14;
        for (int i = 1; i <= counter; i++) {
            if (Utilities.createRandomNumber(10) <= 8) {
                OutputRagingOrk.beBored(RagingUtilities.chooseRandomRagingOrk(this.kommandoz));
                if (Utilities.createRandomNumber(10) <= 5) {
                    this.moshaz.escalate();
                    if (this.moshaz.getRagingLeader().isAngry()) {
                        break;
                    }
                } else {
                    this.ballaz.escalate();
                    if (this.ballaz.getRagingLeader().isAngry()) {
                        break;
                    }
                }
            } else {
                OutputRagingNarrator.peaceJoyPancakes();
            }
        }
        if (!(this.moshaz.getRagingLeader().isAngry() || this.ballaz.getRagingLeader().isAngry())) {
            OutputRagingOrkCamp.niceCanon(this);
        }
    }

    /**
     * Act II:
     *
     * The RagingOrkPack that reached the critical-anger-level starts a fight
     * with the Kommandoz RagingOrkPack. All RagingOrkPacks are moved to certain
     * positions to engage this fight.
     */
    public void startTheFight() {
        if (this.moshaz.getRagingLeader().isAngry()) {
            this.ragingCampground.prepareCombat(this.moshaz, this.ballaz, this);
            this.combat(this.moshaz);
            if (this.kommandoz.getRagingOrks().isEmpty()) {
                this.victoriousPack = this.moshaz;
            } else {
                this.victoriousPack = this.kommandoz;
            }
        } else if (this.ballaz.getRagingLeader().isAngry()) {
            this.ragingCampground.prepareCombat(this.ballaz, this.moshaz, this);
            this.combat(this.ballaz);
            if (this.kommandoz.getRagingOrks().isEmpty()) {
                this.victoriousPack = this.ballaz;
            } else {
                this.victoriousPack = this.kommandoz;
            }
        }
    }

    /**
     * Act III:
     *
     * If the Kommandoz RagingOrkPack was victorious in act II, they turn
     * against the bystanding RagingOrkPack to quench their thirst for blood.
     * Else if the Kommandoz RagingOrkPack has been beaten up, both surviving
     * RagingOrkPacks celebrate the death of their tormentors.
     */
    public void continueTheFight() {
        if (this.moshaz.getRagingOrks().isEmpty()) {
            this.ragingCampground.prepareFinalCombat(ballaz, this);
            this.finalCombat(ballaz);
        } else if (this.ballaz.getRagingOrks().isEmpty()) {
            this.ragingCampground.prepareFinalCombat(moshaz, this);
            this.finalCombat(moshaz);
        }
    }

    /**
     * Act IV:
     *
     * Depending on the outcome of the previous fight-scenes are the surviving
     * instances of RagingOrks clebrating this interesting day.
     */
    public void startTheFeast() {
        if (this.kommandoz.getRagingOrks().isEmpty() & this.moshaz.getRagingOrks().isEmpty()) {
            OutputRagingOrkPack.victory(this.ballaz, this.kommandoz);
        } else if (this.kommandoz.getRagingOrks().isEmpty() & this.ballaz.getRagingOrks().isEmpty()) {
            OutputRagingOrkPack.victory(this.moshaz, this.kommandoz);
        } else if (this.kommandoz.getRagingOrks().isEmpty()) {
            OutputRagingOrkPack.victory(this.victoriousPack, this.kommandoz);
        } else if (this.moshaz.getRagingOrks().isEmpty() & this.ballaz.getRagingOrks().isEmpty()) {
            OutputRagingOrkPack.victory(this.kommandoz, this.moshaz, this.ballaz);
        }
    }

    /**
     * Two instances of RagingOrkPack repeat battle() until one is victorius.
     *
     * @param ragingOrkPack The attacking RagingOrkPack instance, determined by
     * the outcome of act I.
     */
    private void combat(RagingOrkPack ragingOrkPack) {
        while (ragingOrkPack.getRagingOrks().isEmpty() != true) {
            ragingOrkPack.battle(this.getKommandoz());
            this.ragingCampground.displayPlayground(this);
            if (this.kommandoz.getRagingOrks().isEmpty()) {
                break;
            } else if (ragingOrkPack.getRagingOrks().isEmpty()) {
                break;
            }
        }
    }

    /**
     * Two instances of RagingOrkPack repeat battle() until one is victorius.
     *
     * @param ragingOrkPack The attacked RagingOrkPack instance, determined by
     * the outcome of act II.
     */
    private void finalCombat(RagingOrkPack ragingOrkPack) {
        while (this.getKommandoz().getRagingOrks().isEmpty() != true) {
            this.getKommandoz().battle(ragingOrkPack);
            this.ragingCampground.displayPlayground(this);
            if (ragingOrkPack.getRagingOrks().isEmpty()) {
                break;
            } else if (this.getKommandoz().getRagingOrks().isEmpty()) {
                break;
            }
        }
    }

}
