package eu.ggnet.waaagh.rage.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.rage.entity.RagingOrk;
import eu.ggnet.waaagh.rage.entity.RagingOrkPack;
import eu.ggnet.waaagh.rage.tools.RagingUtilities;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by randomly chosen
 * RagingOrk instances inside an RagingOrkPack. Used for The Rage.
 *
 * @author mirko.schulze
 */
public class OutputRagingOrkPack {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputRagingOrkPack() {

    }

    /**
     * Displays a few messages to indicate the end of a fight in act II or act
     * III.
     *
     * @param victoriousPack The victorious RagingOrkPack.
     * @param outgunnedPacks The defeated RagingOrkPack(s).
     */
    public static void victory(RagingOrkPack victoriousPack, RagingOrkPack... outgunnedPacks) {
        OutputRagingOrkLeader.claimVictory(victoriousPack.getRagingLeader());
        OutputRagingOrkPack.anthem(victoriousPack);
        int corpses = 0;
        for (RagingOrkPack orkPack : outgunnedPacks) {
            corpses += orkPack.getRagingLeader().getCharisma();
        }
        for (int i = 0; i < corpses; i++) {
            OutputRagingOrkPack.goCannibal(victoriousPack);
        }
    }

    /**
     * Displays a few messages to indicate a paean following a victory.
     *
     * @param ragingOrkPack The victorious RagingOrkPack instance.
     */
    private static void anthem(RagingOrkPack ragingOrkPack) {
        RagingOrk randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Wir sind da gr\u00f6\u00dften und da st\u00e4rksten!Denn wir sin' da Orkz!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Jeder wei\u00df dat gr\u00fcn am besten is'!Denn wir sin' da Orkz!" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Jeda wird von da Angst gesch\u00fcttelt!Denn wir sin' da Orkz!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Und wenn wir auf'm Weg sin' h\u00e4lt uns nix auf!Denn wir sin' da Orkz!!!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a random message to indicate a feast following a victory.
     *
     * @param ragingOrkPack The victorious OrkPack instance.
     */
    private static void goCannibal(RagingOrkPack ragingOrkPack) {
        RagingOrk randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkPack);
        String output;
        switch (Utilities.createRandomNumber(9)) {
            case 1:
                output = "'Mjamm!Mjamm!Lekka Fleisch!!'";
                break;
            case 2:
                output = "'Zerdr\u00fccken!Zerstampfen!Verschlingen!!'";
                break;
            case 3:
                output = "'Ey Schweinsfresse!!Dat is' mein Fleisch da!!";
                break;
            case 4:
                output = "'Mampf!Mampf!Stopf!Stopf!'";
                break;
            case 5:
                output = "'Mehr Fleisch her!!'";
                break;
            case 6:
                output = "'Da zappelt noch einer!!'";
                break;
            case 7:
                output = "'Guck ma', zuckt da nich' noch was?!'";
                break;
            case 8:
                output = "'Geht doch nix \u00fcber 'ne Orkkeule nach'm Mosh'n!!'";
                break;
            default:
                output = "'Wer hat'n Brenna?!Dann schmekkt's noch bessa!!'";
        }
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }
}
