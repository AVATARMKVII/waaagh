/*
 * Contains entity and output classes that specify the general Ork entity classes. Contains a Utilities class with extra methods for The Rage. Contains a class with the main method.
 */
package eu.ggnet.waaagh.rage;
