package eu.ggnet.waaagh.rage.entity;

import eu.ggnet.waaagh.rage.distinctions.OrkSquads;
import eu.ggnet.waaagh.distinctions.SortsOfBeings;
import eu.ggnet.waaagh.entity.Being;
import eu.ggnet.waaagh.output.OutputBeing;
import eu.ggnet.waaagh.output.OutputNarrator;
import eu.ggnet.waaagh.rage.output.OutputRagingOrk;
import eu.ggnet.waaagh.tools.NamesGeneratorOrk;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * RagingOrk extends Being: extends Being by attribute squad(OrkSquads);
 * contains constructors to instantiate RagingOrk instances as part of an
 * RagingOrkPack instance; only fully initialized instances: used for The Rage.
 *
 * @author mirko.schulze
 */
public class RagingOrk extends Being {

    private OrkSquads squad;

    /**
     * Construcor to initialize instances of RagingOrk in a minimal birth state.
     */
    public RagingOrk() {
        super(SortsOfBeings.ORK);
    }

    /**
     * Constructor to fully initialize an RagingOrk instance as part of an
     * RagingOrkPack instance.
     *
     * @param ragingOrkLeader The leader of the RagingOrkPack instance this RagingOrk
     * instance shall be part of
     */
    public RagingOrk(RagingOrkLeader ragingOrkLeader) {
        this();
        if (ragingOrkLeader.getSquad() == OrkSquads.KOMMANDOZ) {
            super.setName(NamesGeneratorOrk.createBozzName());
            super.setPower(Utilities.createRandomNumber(5) + 10);
            super.setMaxHealth(Utilities.createRandomNumber(5) + 15);
            super.setActHealth(super.getMaxHealth());
        } else {
            super.setName(NamesGeneratorOrk.createOrkName());
            super.setPower(Utilities.createRandomNumber(5) + 5);
            super.setMaxHealth(Utilities.createRandomNumber(5) + 10);
            super.setActHealth(super.getMaxHealth());
        }
        this.squad = ragingOrkLeader.getSquad();
    }

    public OrkSquads getSquad() {
        return squad;
    }

    public void setSquad(OrkSquads squad) {
        this.squad = squad;
    }

    @Override
    public String toString() {
        return super.toString() + "RagingOrk{" + "squad=" + squad + '}';
    }

    @Override
    public void attack(Being being, boolean useRandomizedHits) {
        if (Utilities.hitOrMiss()) {
            OutputRagingOrk.hit(this);
            being.setActHealth(being.getActHealth() - (this.getPower() - being.getResistance()));
            if (being.getActHealth() <= 0) {
                OutputBeing.die(being);
                being.setAlive(false);
            } else {
                OutputBeing.getAttacked(being);
            }
            OutputNarrator.beingAttacksBeing(this, being);
        } else {
            OutputRagingOrk.miss(this);
        }
    }

}
