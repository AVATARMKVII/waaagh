/*
 * Contains copies and extensions of the Ork related classes to be used for The 
 * Rage. Specifies an OrkCamp instance to tell a certain story.
 */
package eu.ggnet.waaagh.rage.entity;
