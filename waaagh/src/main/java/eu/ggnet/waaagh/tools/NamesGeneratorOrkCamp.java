package eu.ggnet.waaagh.tools;

/**
 * Contains a switch-case with 5 names; offers a static method to provide
 * instances of OrkCamp with a random name.
 *
 * @author mirko.schulze
 */
public class NamesGeneratorOrkCamp {

    /**
     * Private constructor to prevent an instantiation.
     */
    private NamesGeneratorOrkCamp() {

    }

    /**
     * Chooses a random String to name an instance of OrkCamp.
     *
     * @return Returns the chosen name.
     */
    public static String createName() {
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                return "Totenfabrik";
            case 2:
                return "Leichenhort";
            case 3:
                return "Blutsumpf";
            case 4:
                return "Höllenblick";
            default:
                return "Grimmzahn";
        }
    }

}
