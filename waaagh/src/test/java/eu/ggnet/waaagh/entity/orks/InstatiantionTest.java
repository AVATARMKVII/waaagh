/*
*
* by Mirko Schulze
 */
package eu.ggnet.waaagh.entity.orks;

import eu.ggnet.waaagh.tools.EntityGenerator;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.assertThat;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class InstatiantionTest {

    public InstatiantionTest() {
    }

    @Test
    public void instantiationTest() {
        OrkCamp orkCamp1 = new OrkCamp();
        OrkCamp orkCamp2 = new OrkCamp();
        OrkCamp orkCamp3 = new OrkCamp();

        //null test orkCamp1
        assertThat(orkCamp1.getName()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp1.getPacks()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp1.getPacks()).as("new camp should be emtpy").isEmpty();
        assertThat(orkCamp1.getCampground()).as("no null at instantiation").isNotNull();

        //null test orkCamp2
        assertThat(orkCamp2.getName()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp2.getPacks()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp2.getPacks()).as("new camp should be emtpy").isEmpty();
        assertThat(orkCamp2.getCampground()).as("no null at instantiation").isNotNull();

        //null test orkCamp3
        assertThat(orkCamp3.getName()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp3.getPacks()).as("no null at instantiation").isNotNull();
        assertThat(orkCamp3.getPacks()).as("new camp should be emtpy").isEmpty();
        assertThat(orkCamp3.getCampground()).as("no null at instantiation").isNotNull();

        //fill orkCamp1
        for (int i = 0; i < 3; i++) {
            List<Ork> weakOrks = new ArrayList<>();
            assertThat(weakOrks).as("new list should be emtpy").isEmpty();
            for (int j = 0; j < 9; j++) {
                weakOrks.add(EntityGenerator.generateGoblin());
                assertThat(weakOrks.size()).as("list size should increase").isEqualTo(j + 1);
            }
            assertThat(weakOrks.size()).as("list size should be 10").isEqualTo(9);
            orkCamp1.getPacks().add(new OrkPack(weakOrks));
            //null test ork pack
            assertThat(orkCamp1.getPacks().get(i).getOrks()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp1.getPacks().get(i).getLeader()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp1.getPacks().get(i).getMembers()).as("no null at instantiation").isNotNull().isNotZero();
            //test setters
            assertThat(orkCamp1.getPacks().get(i).getMembers()).as("members and list size should equal")
                    .isEqualTo(orkCamp1.getPacks().get(i).getOrks().size());
            assertThat(orkCamp1.getPacks().get(i).getLeader()).as("leader should be first ork in list")
                    .isEqualTo(orkCamp1.getPacks().get(i).getOrks().get(0));
            assertThat(orkCamp1.getPacks().size()).as("amount of packs in camp should increase").isEqualTo(i + 1);
        }
        assertThat(orkCamp1.getPacks().size()).as("amount of packs in camp should be 3").isEqualTo(3);

        //fill orkCamp2
        for (int i = 0; i < 5; i++) {
            List<Ork> mediumOrks = new ArrayList<>();
            assertThat(mediumOrks).as("new list should be emtpy").isEmpty();
            for (int j = 0; j < 7; j++) {
                mediumOrks.add(EntityGenerator.generateGoblin());
                assertThat(mediumOrks.size()).as("list size should increase").isEqualTo(j + 1);
            }
            assertThat(mediumOrks.size()).as("list size should be 10").isEqualTo(7);
            orkCamp2.getPacks().add(new OrkPack(mediumOrks));
            //null test ork pack
            assertThat(orkCamp2.getPacks().get(i).getOrks()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp2.getPacks().get(i).getLeader()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp2.getPacks().get(i).getMembers()).as("no null at instantiation").isNotNull().isNotZero();
            //test setters
            assertThat(orkCamp2.getPacks().get(i).getMembers()).as("members and list size should equal")
                    .isEqualTo(orkCamp2.getPacks().get(i).getOrks().size());
            assertThat(orkCamp2.getPacks().get(i).getLeader()).as("leader should be first ork in list")
                    .isEqualTo(orkCamp2.getPacks().get(i).getOrks().get(0));
            assertThat(orkCamp2.getPacks().size()).as("amount of packs in camp should increase").isEqualTo(i + 1);
        }
        assertThat(orkCamp2.getPacks().size()).as("amount of packs in camp should be 3").isEqualTo(5);

        //fill orkCamp3
        for (int i = 0; i < 2; i++) {
            List<Ork> strongOrks = new ArrayList<>();
            assertThat(strongOrks).as("new list should be emtpy").isEmpty();
            for (int j = 0; j < 15; j++) {
                strongOrks.add(EntityGenerator.generateGoblin());
                assertThat(strongOrks.size()).as("list size should increase").isEqualTo(j + 1);
            }
            assertThat(strongOrks.size()).as("list size should be 10").isEqualTo(15);
            orkCamp3.getPacks().add(new OrkPack(strongOrks));
            //null test ork pack
            assertThat(orkCamp3.getPacks().get(i).getOrks()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp3.getPacks().get(i).getLeader()).as("no null at instantiation").isNotNull();
            assertThat(orkCamp3.getPacks().get(i).getMembers()).as("no null at instantiation").isNotNull().isNotZero();
            //test setters
            assertThat(orkCamp3.getPacks().get(i).getMembers()).as("members and list size should equal")
                    .isEqualTo(orkCamp3.getPacks().get(i).getOrks().size());
            assertThat(orkCamp3.getPacks().get(i).getLeader()).as("leader should be first ork in list")
                    .isEqualTo(orkCamp3.getPacks().get(i).getOrks().get(0));
            assertThat(orkCamp3.getPacks().size()).as("amount of packs in camp should increase").isEqualTo(i + 1);
        }
        assertThat(orkCamp3.getPacks().size()).as("amount of packs in camp should be 3").isEqualTo(2);
    }

}
