package eu.ggnet.waaagh.rage.tools;

import eu.ggnet.waaagh.rage.entity.RagingOrk;
import eu.ggnet.waaagh.rage.entity.RagingOrkCamp;
import eu.ggnet.waaagh.rage.entity.RagingOrkCampground;
import eu.ggnet.waaagh.rage.entity.RagingOrkLeader;
import eu.ggnet.waaagh.rage.entity.RagingOrkPack;
import eu.ggnet.waaagh.tools.Utilities;
import static eu.ggnet.waaagh.tools.Utilities.createRandomNumber;

/**
 * Contains some extra static methods for The Rage.
 *
 * @author mirko.schulze
 */
public class RagingUtilities {

    /**
     * Selects a random RagingOrk instance in an RagingOrkPack instance.
     *
     * @param ragingOrkPack The RagingOrkPack that contains the RagingOrk.
     * @return Returns the chosen RagingOrk.
     */
    public static RagingOrk chooseRandomRagingOrk(RagingOrkPack ragingOrkPack) {
        return ragingOrkPack.getRagingOrks().get(Utilities.createRandomIndex(ragingOrkPack.getRagingOrks()));
    }

    /**
     * Selects a random RagingOrk instance in an RagingOrkCamp instance.
     *
     * @param ragingOrkCamp The RagingOrkCamp that contains the RagingOrk.
     * @return Returns the chosen RagingOrk.
     */
    public static RagingOrk chooseRandomRagingOrk(RagingOrkCamp ragingOrkCamp) {
        RagingOrkPack randomPack = ragingOrkCamp.getRagingPacks().get(Utilities.createRandomIndex(ragingOrkCamp.getRagingPacks()));
        return chooseRandomRagingOrk(randomPack);
    }

    /**
     * Selects a random OrkLeader instance in an RagingOrkCamp instance.
     *
     * @param ragingOrkCamp The RagingOrkCamp that contains the RagingOrkLeader.
     * @return Returns the chosen RagingOrkLeader.
     */
    public static RagingOrkLeader chooseRandomRagingLeader(RagingOrkCamp ragingOrkCamp) {
        return ragingOrkCamp.getRagingPacks().get(Utilities.createRandomIndex(ragingOrkCamp.getRagingPacks())).getRagingLeader();
    }

    /**
     * Creates a number in the bounds of the size of the RagingOrkCampground to
     * set the x coordinate for a Point object.
     *
     * @param ragingOrkCampground The RagingOrkCampground on which a coordinate
     * is needed.
     * @return Returns the created number.
     */
    public static int createRandomX(RagingOrkCampground ragingOrkCampground) {
        int min = 0;
        int max = ragingOrkCampground.getWidth();
        int random = createRandomNumber(max);
        if (random == (min | min + 1)) {
            return random + 2;
        } else if (random == (max | max - 1)) {
            return random - 2;
        } else {
            return random;
        }
    }

    /**
     * Creates a number in the bounds of the size of the RagingOrkCampground to
     * set the y coordinate for a Point object.
     *
     * @param ragingOrkCampground The RagingOrkCampground on which a coordinate
     * is needed.
     * @return Returns the created number.
     */
    public static int createRandomY(RagingOrkCampground ragingOrkCampground) {
        int min = 0;
        int max = ragingOrkCampground.getHeight();
        int random = createRandomNumber(max);
        if (random == min) {
            return random + 1;
        } else if (random == max) {
            return random - 2;
        } else {
            return random;
        }
    }

}
