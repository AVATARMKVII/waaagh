package eu.ggnet.waaagh.entity.orks;

import java.util.ArrayList;
import java.util.Arrays;
import eu.ggnet.waaagh.tools.NamesGeneratorOrkCamp;
import eu.ggnet.waaagh.interfaces.Presentable;
import eu.ggnet.waaagh.tools.EntityGenerator;

/**
 * Brings multiple OrkPack instances together. Contains attributes name(String),
 * packs(ArrayList) and campground(OrkCampground); only fully initialized
 * instances.
 *
 * @author mirko.schulze
 */
public class OrkCamp implements Presentable {

    private final String name;
    private final ArrayList<OrkPack> packs;
    private final OrkCampground campground;

    /**
     * Constructor to fully initialize an instance of OrkCamp.
     */
    public OrkCamp() {
        this.name = NamesGeneratorOrkCamp.createName();
        this.packs = new ArrayList<>();
        this.campground = new OrkCampground();
    }

    /**
     * Constructor to form an OrkCamp instance out of OrkPack instances.
     *
     * @param orkPack The OrkPack that shall found this OrkCamp.
     * @param orkPacks The OrkPacks that shall populate this OrkCamp.
     */
    public OrkCamp(OrkPack orkPack, OrkPack... orkPacks) {
        this();
        this.packs.add(orkPack);
        this.packs.addAll(Arrays.asList(orkPacks));
        this.campground.submitAndDeletePositionsCamp(this);
    }

    /**
     * Constructor to fully initialize an instance of OrkCamp with a specified
     * number of OrkPack instances.
     *
     * @param amountOfPacks Total number of instances of OrkPack in this
     * instance of OrkCamp.
     */
    public OrkCamp(int amountOfPacks) {
        this();
        for (int i = 0; i < amountOfPacks; i++) {
            this.packs.add(EntityGenerator.generateOrkPack());
        }
        this.campground.submitAndDeletePositionsCamp(this);
    }

    public String getName() {
        return name;
    }

    public ArrayList<OrkPack> getPacks() {
        return packs;
    }

    public OrkCampground getCampground() {
        return campground;
    }

    @Override
    public String toString() {
        return "OrkCamp{" + "name=" + name + ", packs=" + packs + ", campGround=" + campground + '}';
    }

    @Override
    public void present() {
        this.packs.forEach(oP -> {
            oP.getOrks().forEach(o -> {
                o.present();
            });
        });
    }

}
