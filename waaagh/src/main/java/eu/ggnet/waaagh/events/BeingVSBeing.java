package eu.ggnet.waaagh.events;

import eu.ggnet.waaagh.entity.Being;
import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.entity.orks.OrkPack;
import java.util.List;
import eu.ggnet.waaagh.output.OutputNarrator;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains methods to let instances of Being engage in a fight.
 *
 * @author mirko.schulze
 */
public class BeingVSBeing {

    /**
     * Two instances of Being call attack on turns.
     *
     * @param being1 The first atacking Being.
     * @param being2 The first attacked Being.
     */
    public static void attackAndCounter(Being being1, Being being2) {
        being1.attack(being2, true);
        OutputNarrator.beingAttacksBeing(being1, being2);
        if (being2.getActHealth() > 0) {
            being2.attack(being1, true);
            OutputNarrator.beingAttacksBeing(being2, being1);
        }
    }

    /**
     * Two instances of Being call attack until one is dead.
     *
     * @param being1 The first attacking Being.
     * @param being2 The first attacked Being.
     */
    public static void fightTillDeath(Being being1, Being being2) {
        while (being1.getActHealth() > 0) {
            being1.attack(being2, true);
            OutputNarrator.beingAttacksBeing(being1, being2);
            if (being2.getActHealth() > 0) {
                being2.attack(being1, true);
                OutputNarrator.beingAttacksBeing(being2, being1);
            } else {
                break;
            }
        }
    }

    /**
     * An instance of Being calls attack against a List full of instances of
     * Being.
     *
     * @param being
     * @param beings
     */
    public static void fightTheWorld(Being being, List<Being> beings) {
        while (being.getActHealth() > 0 & !beings.isEmpty()) {
            Being randomBeing = beings.get(Utilities.createRandomIndex(beings));
            being.attack(randomBeing, true);
            if (!randomBeing.isAlive()) {
                beings.remove(randomBeing);
                OutputNarrator.killstreak(being);
                fightTheWorld(being, beings);
            } else {
                randomBeing.attack(being, true);
                for (byte b = 1; b <= 4; b++) {
                    Being anotherRandomBeing = beings.get(Utilities.createRandomIndex(beings));
                    anotherRandomBeing.attack(being, true);
                }
            }
            System.out.println(being.toString());
            System.out.println(beings.toString());
            System.out.println(randomBeing.toString());
        }
    }

    /**
     *
     * @param ork
     * @param orkPack
     */
    public static void fightTheWorld(Ork ork, OrkPack orkPack) {
        while (ork.getActHealth() > 0 & !orkPack.getOrks().isEmpty()) {
            Ork randomBeing = orkPack.getOrks().get(Utilities.createRandomIndex(orkPack.getOrks()));
            ork.attack(randomBeing, true);
            if (!randomBeing.isAlive()) {
                orkPack.getOrks().remove(randomBeing);
                OutputNarrator.killstreak(ork);
                fightTheWorld(ork, orkPack);
            } else {
                randomBeing.attack(ork, true);
                for (byte b = 1; b <= 4; b++) {
                    Being anotherRandomBeing = orkPack.getOrks().get(Utilities.createRandomIndex(orkPack.getOrks()));
                    anotherRandomBeing.attack(ork, true);
                }
            }
            System.out.println(ork.toString());
        }
    }

    public static void listVSList(List<Ork> beings1, List<Ork> beings2) {
        beings1.forEach(o -> o.attackAndCounter(beings2.get(Utilities.createRandomIndex(beings2)), true));
        beings2.removeIf(o -> o.getActHealth() <= 0);
    }

}
