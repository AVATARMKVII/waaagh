/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.PackController;

import eu.ggnet.waaagh.entity.orks.OrkPack;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author mirko.schulze
 */
@Named
@ViewScoped
public class GobPackController implements Serializable {

    private ArrayList<OrkPack> orkPacks;

    public ArrayList<OrkPack> getOrkPacks() {
        return orkPacks;
    }

    public void setOrkPacks(ArrayList<OrkPack> orkPacks) {
        this.orkPacks = orkPacks;
    }

    @PostConstruct
    public void orkPacksInit() {
        this.orkPacks = new ArrayList<>();
        for (byte b = 1; b <= 10; b++) {
            this.orkPacks.add(EntityGenerator.generateWaaaghGuard());
        }
    }

}
