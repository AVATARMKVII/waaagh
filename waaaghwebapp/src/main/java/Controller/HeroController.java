/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import eu.ggnet.waaagh.distinctions.Genders;
import eu.ggnet.waaagh.entity.Hero;
import java.io.Serializable;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author mirko.schulze
 */
@Named
@ViewScoped
public class HeroController implements Serializable {
    
    private Hero hero;

    public Hero getHero() {
        return hero;
    }

    public void setHero(Hero hero) {
        this.hero = hero;
    }
    
    @PostConstruct
    public void heroInit(){
        this.hero = new Hero(Genders.MALE);
    }
    
    
}
