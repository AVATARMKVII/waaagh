/*
*
* by Mirko Schulze
 */
package eu.ggnet.waaagh.tools;

import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class UtilitiesTest {

    public UtilitiesTest() {
    }

    /**
     * Test of createRandomNumber method, of class Utilities.
     */
    @Test
    public void testCreateRandomNumber() {
        assertThat(Utilities.createRandomNumber(1)).as("randy should be 1").isEqualTo(1);
        for (int i = 0; i < 100; i++) {
            assertThat(Utilities.createRandomNumber(10)).as("randy should be greater 0 and less/equal 10")
                    .isBetween(1, 10);
        }
    }

    /**
     * Test of createRandomIndex method, of class Utilities.
     */
    @Test
    public void testCreateRandomIndex() {
        List<String> strings = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            strings.add("String Nr." + i);
        }
        for (int i = 0; i < 100; i++) {
            assertThat(Utilities.createRandomIndex(strings)).as("randy should be greater/equal 0 and less 5");
        }
    }

}
