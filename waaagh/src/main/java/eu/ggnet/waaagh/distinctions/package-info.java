/**
 * Contains classes and enums to differentiate between instances.
 */
package eu.ggnet.waaagh.distinctions;
