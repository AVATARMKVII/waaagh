package eu.ggnet.waaagh.output;

import static eu.ggnet.waaagh.distinctions.Colours.ANSI_RESET;
import eu.ggnet.waaagh.entity.Being;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by a Being.
 *
 * @author Administrator
 */
public class OutputBeing {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputBeing() {

    }

    /**
     * Displays a message to indicate an instance of Being is getting attacked.
     *
     * @param being The attacked Being.
     */
    public static void getAttacked(Being being) {
        String output;
        switch (Utilities.createRandomNumber(3)) {
            case 1:
                output = "'Autsch!'";
                break;
            case 2:
                output = "'Aahh!!'";
                break;
            default:
                output = "'Du Hund!'";
        }
        System.out.println(being.getName() + ": " + being.getSort().getColour() + output + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a succesful attack.
     *
     * @param being The attacking Being.
     */
    public static void attack(Being being) {
        String output;
        switch (Utilities.createRandomNumber(3)) {
            case 1:
                output = "'Treffer'";
                break;
            case 2:
                output = "'Wie schmeckt das?'";
                break;
            default:
                output = "'Viel spaß damit!'";
        }
        System.out.println(being.getName() + ": " + being.getSort().getColour() + output + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a failing attack.
     *
     * @param being The attacking Being.
     */
    public static void miss(Being being) {
        String output;
        switch (Utilities.createRandomNumber(3)) {
            case 1:
                output = "'Mist'";
                break;
            case 2:
                output = "'Verdammt'";
                break;
            default:
                output = "'Daneben'";
        }
        System.out.println(being.getName() + ": " + being.getSort().getColour() + output + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate an instance of Being is dying.
     *
     * @param being The dying Being.
     */
    public static void die(Being being) {
        String output;
        switch (Utilities.createRandomNumber(2)) {
            case 1:
                output = "'..oOOooo...o.'";
                break;
            default:
                output = "'Glugg, bluub...röchel..'";
        }
        System.out.println(being.getName() + ": " + being.getSort().getColour() + output + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
