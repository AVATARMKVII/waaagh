/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package waaagh.waaaghapp;

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author mirko.schulze
 */
@ManagedBean
@RequestScoped
public class BrowserTabTitle {

    private String title = "Waaagh";

    public String getTitle() {
        return title;
    }

    public void setTitle(String indexMessage) {
        this.title = indexMessage;
    }

}
