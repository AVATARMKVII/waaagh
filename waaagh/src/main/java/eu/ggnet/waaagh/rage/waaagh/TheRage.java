package eu.ggnet.waaagh.rage.waaagh;

import eu.ggnet.waaagh.rage.entity.RagingOrkCamp;
import eu.ggnet.waaagh.rage.output.OutputRagingNarrator;

/**
 * Contains the main method. Connects the methods offered by RagingOrkCamp and
 * OutputRagingNarrator to tell a certain story.
 *
 * @author mirko.schulze
 */
public class TheRage {

    public static void main(String[] args) {

        RagingOrkCamp roc = new RagingOrkCamp();

        //The Rage
        //Act I
        OutputRagingNarrator.introduction(roc);
        OutputRagingNarrator.actOne();
        roc.startTheRage();

        //Act II
        OutputRagingNarrator.actTwo();
        roc.startTheFight();

        //Act III
        OutputRagingNarrator.actThree(roc);
        roc.continueTheFight();

        //Act IV
        OutputRagingNarrator.actFour();
        roc.startTheFeast();
        OutputRagingNarrator.outroduction();

    }

}
