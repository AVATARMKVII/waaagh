/**
 * Contains an enum to differentiate between instances of RagingOrkPack in The Rage.
 */
package eu.ggnet.waaagh.rage.distinctions;
