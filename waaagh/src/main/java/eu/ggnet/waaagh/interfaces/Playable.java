package eu.ggnet.waaagh.interfaces;

/**
 * An testing interface.
 *
 * @author mirko.schulze
 */
@FunctionalInterface
public interface Playable {

    abstract boolean playMe();

}
