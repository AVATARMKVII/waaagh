package eu.ggnet.waaagh.entity;

import eu.ggnet.waaagh.distinctions.SortsOfBeings;
import eu.ggnet.waaagh.interfaces.Presentable;
import java.awt.Point;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Semi-abstract class Being implements Comparable(Being), Presentable: contains
 * attributes sort(SortsOfBeings), alive(boolean), name(String), power(int),
 * maxHealth(int), actHealth(int), resistance(int) and position(Point); offers
 * the abstract method attack(Being) and the concrete methods
 * attackAndCounter(Being) and fightUntilDeath(Being); defines compareTo(Being)
 * as a comparison of power/maxHealth ratios.
 *
 * @author mirko.schulze
 */
public abstract class Being implements Comparable<Being>, Presentable {

    private final SortsOfBeings sort;
    private boolean alive;
    private String name;
    private int power;
    private int maxHealth;
    private int actHealth;
    private int resistance;
    private Point position;

    /**
     * Constructor to initialize instances of Being in a minimal birth state.
     *
     * @param sort The visualization of this instance depends on the sort.
     */
    public Being(SortsOfBeings sort) {
        this.sort = sort;
        this.alive = true;
    }

    public SortsOfBeings getSort() {
        return sort;
    }

    public boolean isAlive() {
        return alive;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public int getMaxHealth() {
        return maxHealth;
    }

    public int getActHealth() {
        return actHealth;
    }

    public int getResistance() {
        return resistance;
    }

    public Point getPosition() {
        return position;
    }

    public void setAlive(boolean alive) {
        this.alive = alive;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public void setMaxHealth(int maxHealth) {
        this.maxHealth = maxHealth;
    }

    public void setActHealth(int actHealth) {
        this.actHealth = actHealth;
    }

    public void setResistance(int resistance) {
        this.resistance = resistance;
    }

    public void setPosition(Point position) {
        this.position = position;
    }

    public void setXCoordinate(int coordinate) {
        this.position.x = coordinate;
    }

    public void setYCoordinate(int coordinate) {
        this.position.y = coordinate;
    }

    @Override
    public String toString() {
        return "Being{" + "sort=" + sort + ", alive=" + alive
                + ", name=" + name + ", power=" + power
                + ", maxHealth=" + maxHealth + ", actHealth=" + actHealth
                + ", resistance=" + resistance + ", position=" + position + '}';
    }

    /**
     * Defines the comparison between two instances of Being by comparing their
     * power.
     *
     * @param t The Being that this instance of Being shall be compared to.
     * @return Returns -1 if this instance's power/maxHealth ratio is higher, 1
     * if it is lower.
     */
    @Override
    public int compareTo(Being t) {
        return this.power < t.power ? -1 : (this.power > t.power ? 1 : 0);
    }

    @Override
    public void present() {
        System.out.println("Name = " + this.getName() + ", Kraft = " + this.getPower()
                + ", Gesundheit = " + this.getActHealth() + "/" + this.getMaxHealth()
                + ", Widerstand = " + this.getResistance());
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Abstract method. An instance of Being attacks another instance of Being.
     * The attacking instance causes damage equal to it's power. The attacked
     * instance loses health equal to the inflicted damage reduced by it's
     * resistance. If the attacked instance's health decreases to zero or below,
     * boolean alive becomes false.
     *
     * @param being The instance of Being that is getting attacked.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public abstract void attack(Being being, boolean useRandomizedHits);

    /**
     * An instance of Being calls attack upon another instance of Being. If the
     * attacked Being is still alive after the attack, it will call attack upon
     * the attacking instance of Being to counter.
     *
     * @param being The instance of Being that is getting attacked.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public void attackAndCounter(Being being, boolean useRandomizedHits) {
        this.attack(being, useRandomizedHits);
        if (being.isAlive()) {
            being.attack(this, useRandomizedHits);
        }
    }

    /**
     * An instance of Being calls attackAndCounter upon another instance of
     * Being. This is repeated until one of them is dead.
     *
     * @param being The instance of Being that is getting attacked.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public void fightUntilDeath(Being being, boolean useRandomizedHits) {
        while (this.isAlive() & being.isAlive()) {
            this.attackAndCounter(being, useRandomizedHits);
        }
    }

}
