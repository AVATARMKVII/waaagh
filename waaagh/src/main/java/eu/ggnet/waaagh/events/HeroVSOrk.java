package eu.ggnet.waaagh.events;

import eu.ggnet.waaagh.entity.Hero;
import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.output.OutputHero;
import eu.ggnet.waaagh.output.OutputNarrator;
import eu.ggnet.waaagh.output.OutputOrk;
import eu.ggnet.waaagh.tools.EntityGenerator;
import eu.ggnet.waaagh.tools.Utilities;

/**
 *
 * @author mirko.schulze
 */
public class HeroVSOrk {

    /**
     * Starts a fight between a instance of Hero and a new Ork instance.
     *
     * @param hero The fighting Hero
     */
    public static void wildOrkAppears(Hero hero) {
        battle(hero, EntityGenerator.generateOrk());
    }

    /**
     * An instance of Hero and and an instance of Ork repeat fight(Hero hero,
     * Ork ork) until one of them is dead.
     *
     * @param hero The fighting Hero
     * @param ork The fighting Ork
     * @see fight(Hero hero, Ork ork)
     */
    private static void battle(Hero hero, Ork ork) {
        while (hero.getActHealth() > 0 && ork.getActHealth() > 0) {
            fight(hero, ork);
        }
        if (!ork.isAlive()) {
            hero.gainExp(30);
        }
    }

    /**
     * An instance of Hero attacks an instance of Ork. If Ork survives the
     * attack, it starts a counterattack.
     *
     * @param hero The fighting Hero
     * @param ork The fighting Ork
     */
    private static void fight(Hero hero, Ork ork) {
        if (Utilities.hitOrMiss()) {
            hero.attack(ork, true);
            OutputNarrator.beingAttacksBeing(hero, ork);
            if(ork.getActHealth() <= 0){
                OutputOrk.die(ork);
            }
        } else {
            OutputHero.miss(hero);
        }
        if (ork.getActHealth() > 0) {
            if (Utilities.hitOrMiss()) {
                ork.attack(hero, true);
                OutputNarrator.beingAttacksBeing(ork, hero);
                if(hero.getActHealth() <= 0){
                    OutputHero.die(hero);
                }
            } else {
                OutputOrk.miss(ork);
            }
        }
    }

}
