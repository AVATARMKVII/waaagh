package eu.ggnet.waaagh.entity.orks;

import eu.ggnet.waaagh.distinctions.SortsOfBeings;
import eu.ggnet.waaagh.entity.Being;
import eu.ggnet.waaagh.output.OutputBeing;
import eu.ggnet.waaagh.output.OutputOrk;
import eu.ggnet.waaagh.tools.Utilities;
import eu.ggnet.waaagh.output.OutputNarrator;

/**
 * Ork extends Being: extends Being by attribute type(string); only initialized
 * instances.
 *
 * @author mirko.schulze
 */
public class Ork extends Being {

    private String type;

    /**
     * Constructor to fully initialize an Ork instance.
     *
     * @param type Used to differentiate between differekt Orks.
     * @param name Used to set this instance's name.
     * @param power Used to set this instance's power.
     * @param maxHealth Used to set this instance's maxHealth.
     * @param resistance Used to set this instance's resistance.
     * @see tools.EntityGenerator#generateOrk()
     */
    public Ork(String type, String name, int power, int maxHealth, int resistance) {
        super(SortsOfBeings.ORK);
        this.type = type;
        super.setName(name);
        super.setPower(power);
        super.setMaxHealth(maxHealth);
        super.setActHealth(super.getMaxHealth());
        super.setResistance(resistance);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return super.toString() + "Ork{" + "type=" + type + '}';
    }

    @Override
    public void attack(Being being, boolean useRandomizedHits) {
        if (useRandomizedHits) {
            if (Utilities.hitOrMiss()) {
                OutputOrk.hit(this);
                being.setActHealth(being.getActHealth() - (this.getPower() - being.getResistance()));
                if (being.getActHealth() <= 0) {
                    OutputBeing.die(being);
                    being.setAlive(false);
                } else {
                    OutputBeing.getAttacked(being);
                }
                OutputNarrator.beingAttacksBeing(this, being);
            } else {
                OutputOrk.miss(this);
            }
        } else {
            OutputOrk.hit(this);
            being.setActHealth(being.getActHealth() - (this.getPower() - being.getResistance()));
            if (being.getActHealth() <= 0) {
                OutputBeing.die(being);
                being.setAlive(false);
            } else {
                OutputBeing.getAttacked(being);
            }
            OutputNarrator.beingAttacksBeing(this, being);
        }

    }

}
