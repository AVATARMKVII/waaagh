/*
*
* by Mirko Schulze
 */
package eu.ggnet.waaagh.entity.orks;

import eu.ggnet.waaagh.tools.EntityGenerator;
import java.util.ArrayList;
import java.util.List;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class OrkCampgroundTest {

    OrkCampground campground;

    public OrkCampgroundTest() {
    }

    @Before
    public void setUp() {
        campground = new OrkCampground();
    }

    /**
     * Test of submitAndDeletePositionOrk method, of class OrkCampground.
     */
    @Test
    public void testSubmitAndDeletePositionOrk() {
        Ork ork = EntityGenerator.generateOrk();
        int originalSize = campground.getPositions().size();
        campground.submitAndDeletePositionOrk(ork);
        assertThat(campground.getPositions().size()).as("positions size should be 1 lower")
                .isNotSameAs(originalSize).isLessThan(originalSize).isStrictlyBetween(originalSize - 2, originalSize);
        assertThat(ork.getPosition().x).as("orks x coordinate should be inside of the campground boundaries")
                .isStrictlyBetween(campground.getMinWidth(), campground.getMaxWidth());
        assertThat(ork.getPosition().y).as("ork y coordinate should be inside of the campground boundaries")
                .isStrictlyBetween(campground.getMinHeight(), campground.getMaxHeight());
    }

    /**
     * Test of submitAndDeletePositionPack method, of class OrkCampground.
     */
    @Test
    public void testSubmitAndDeletePositionPack() {
        List<Ork> orks = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            orks.add(EntityGenerator.generateOrk());
        }
        OrkPack pack = new OrkPack(orks);
        int originalSize = campground.getPositions().size();
        campground.submitAndDeletePositionPack(pack);
        assertThat(campground.getPositions().size()).as("positions size should be " + pack.getMembers() + " lower")
                .isEqualTo(originalSize - pack.getMembers());
        for (Ork ork : orks) {
            assertThat(ork.getPosition().x).as("orks x coordinate should be inside of the campground boundaries")
                    .isStrictlyBetween(campground.getMinWidth(), campground.getMaxWidth());
            assertThat(ork.getPosition().y).as("ork y coordinate should be inside of the campground boundaries")
                    .isStrictlyBetween(campground.getMinHeight(), campground.getMaxHeight());
        }
    }

    /**
     * Test of submitAndDeletePositionsCamp method, of class OrkCampground.
     */
    @Test
    public void testSubmitAndDeletePositionsCamp() {
        OrkCamp camp = new OrkCamp(5);
        int originalSize = campground.getPositions().size();
        campground.submitAndDeletePositionsCamp(camp);
        int allMembers = 0;
        for (OrkPack pack : camp.getPacks()) {
            allMembers += pack.getMembers();
        }
        assertThat(campground.getPositions().size()).as("positions size should be " + allMembers + " lower")
                .isEqualTo(originalSize - allMembers);
        for (OrkPack pack : camp.getPacks()) {
            for (Ork ork : pack.getOrks()) {
                assertThat(ork.getPosition().x).as("orks x coordinate should be inside of the campground boundaries")
                        .isStrictlyBetween(campground.getMinWidth(), campground.getMaxWidth());
                assertThat(ork.getPosition().y).as("ork y coordinate should be inside of the campground boundaries")
                        .isStrictlyBetween(campground.getMinHeight(), campground.getMaxHeight());
            }
        }
    }

}
