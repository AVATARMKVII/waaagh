/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ring;

import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.inject.Named;

/**
 *
 * @author Mirko Schulze <mirko.schulze at gg-net.eu>
 */
@Named
public class RingView implements Serializable {
    
    private ArrayList<Ork> orks;
    private Ork selectedOrk;

    public ArrayList<Ork> getOrks() {
        return orks;
    }

    public void setOrks(ArrayList<Ork> orks) {
        this.orks = orks;
    }

    public Ork getSelectedOrk() {
        return selectedOrk;
    }

    public void setSelectedOrk(Ork selectedOrk) {
        this.selectedOrk = selectedOrk;
    }
    
    @PostConstruct
    public void orksInit(){
        orks = new ArrayList<>();
        orks.add(EntityGenerator.generateGoblin());
        orks.add(EntityGenerator.generateOrk());
        orks.add(EntityGenerator.generateBlackOrk());
        orks.add(EntityGenerator.generateBigUn());
        orks.add(EntityGenerator.generateWaaaghBozz());
    }
    
}
