/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.OrkController;

import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author mirko.schulze
 */
@Named
@ViewScoped
public class BlackOrkController implements Serializable{
    
    private ArrayList<Ork> blackOrks;

    public ArrayList<Ork> getBlackOrks() {
        return blackOrks;
    }

    public void setBlackOrks(ArrayList<Ork> blackOrks) {
        this.blackOrks = blackOrks;
    }
    
    @PostConstruct
    public void blackOrksInit(){
        this.blackOrks = new ArrayList<>();
        for(byte b = 0; b <= 5;b++){
            this.blackOrks.add(EntityGenerator.generateBlackOrk());
        }
    }
    
    
}
