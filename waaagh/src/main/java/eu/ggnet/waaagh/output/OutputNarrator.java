package eu.ggnet.waaagh.output;

import static eu.ggnet.waaagh.distinctions.Colours.*;
import eu.ggnet.waaagh.entity.Being;
import eu.ggnet.waaagh.entity.Hero;
import eu.ggnet.waaagh.entity.Narrator;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by the narrator to
 * indicate different actions.
 *
 * @author mirko.schulze
 */
public class OutputNarrator {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputNarrator() {

    }

    /**
     * Streams a few messages to indicate a succesful attack made by an instance
     * of Being and the results.
     *
     * @param attacker The attacking Being.
     * @param defender The attacked Being.
     */
    public static void beingAttacksBeing(Being attacker, Being defender) {
        System.out.println(Narrator.getNARRATOR() + attacker.getName() + " trifft "
                + defender.getName() + ", " + defender.getName() + " verliert "
                + ((attacker.getPower() - defender.getResistance()) >= 0 ? attacker.getPower() - defender.getResistance() : 0)
                + " Gesundheit.'" + ANSI_RESET);
        Utilities.sleepForASecond();
        if (defender.getActHealth() > 0) {
            System.out.println(Narrator.getNARRATOR() + defender.getName() + " hat noch " + defender.getActHealth() + " Gesundheit.'" + ANSI_RESET);
        } else if (defender.getActHealth() <= 0) {
            System.out.println(Narrator.getNARRATOR() + defender.getName() + " ist tot.'" + ANSI_RESET);
        }
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a message to indicate a missing attack.
     *
     * @param being The attacking Being.
     */
    public static void heMissed(Being being) {
        System.out.println(Narrator.getNARRATOR() + being.getName() + " hat verfehlt...'" + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a message to indicate an instance of Being is dying.
     *
     * @param being The dying Being.
     */
    public static void heIsDead(Being being) {
        System.out.println(Narrator.getNARRATOR() + being.getName() + " ist von uns gegangen...'" + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a message to indicate an instance of Being unleashing a series of
     * lethal attacks.
     *
     * @param being The attacking Being.
     */
    public static void killstreak(Being being) {
        System.out.println(Narrator.getNARRATOR() + being.getName() + " nimmt ordentlich Anlauf und...'" + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "PAIN-TRAIN!!!'");
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a message to indicate an instance of Hero is gaining exp.
     *
     * @param hero The benefitted Hero.
     * @param exp The amount of exp gained by the Hero.
     */
    public static void heroGainsExp(Hero hero, int exp) {
        System.out.println(Narrator.getNARRATOR() + hero.getName() + " erhält " + exp + " Ehrfarungspunkte.'" + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a message to indicate an instance of Hero increasing his level.
     *
     * @param hero The promoted Hero.
     */
    public static void heroLevelsUp(Hero hero) {
        System.out.println(Narrator.getNARRATOR() + hero.getName() + " hat Stufe " + hero.getLvl() + " erreicht!" + ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
