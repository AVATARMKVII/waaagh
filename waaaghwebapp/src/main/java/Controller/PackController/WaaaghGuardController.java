/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.PackController;

import eu.ggnet.waaagh.entity.orks.OrkPack;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author mirko.schulze
 */
@Named
@ViewScoped
public class WaaaghGuardController implements Serializable {

    private OrkPack orkPack;

    public OrkPack getOrkPack() {
        return orkPack;
    }

    public void setOrkPack(OrkPack orkPacks) {
        this.orkPack = orkPacks;
    }

    @PostConstruct
    public void orkPacksInit() {
        this.orkPack = EntityGenerator.generateWaaaghGuard();
    }

    public ArrayList<String> getAllNames() {
        ArrayList<String> names = new ArrayList<>();
        this.orkPack.getOrks().forEach(o -> names.add(o.getName()));
        return names;
    }
}


