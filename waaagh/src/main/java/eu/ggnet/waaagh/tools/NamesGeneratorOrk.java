package eu.ggnet.waaagh.tools;

/**
 * Contains switch-cases with fifty names and fifty titles; offers static
 * methods to provide instances of Ork with a random name.
 *
 * @author mirko.schulze
 */
public class NamesGeneratorOrk {

    /**
     * Private constructor to prevent an instantiation.
     */
    private NamesGeneratorOrk() {

    }

    /**
     * Chooses a random String from generateName().
     *
     * @return Returns the chosen name.
     */
    public static String createGobName() {
        return generateName();
    }

    /**
     * Combines the random Strings from createName() and createTitle().
     *
     * @return Returns the built name.
     */
    public static String createOrkName() {
        return generateName() + " " + generateTitle();
    }

    /**
     * Adds "Bozz" to createOrkName().
     *
     * @return Returns the built name.
     */
    public static String createBozzName() {
        return "Bozz " + createOrkName();
    }

    /**
     * Adds "Megabozz" to createOrkName().
     *
     * @return Returns the built name.
     */
    public static String createWaaaghBozzName() {
        return "Waaagh Bozz " + createOrkName();
    }

    /**
     * Randomly chooses a name for createGobName() and createOrkName().
     *
     * @return Returns the chosen name.
     */
    private static String generateName() {
        switch (Utilities.createRandomNumber(50)) {
            case 1:
                return "Naarbluzh";
            case 2:
                return "Grondakk";
            case 3:
                return "Snagga-snagga";
            case 4:
                return "Krazzhakk";
            case 5:
                return "Krizdragas";
            case 6:
                return "Smukgugus";
            case 7:
                return "Durkug";
            case 8:
                return "Buzdrogg";
            case 9:
                return "Gharzakk";
            case 10:
                return "Truskork";
            case 11:
                return "Argam";
            case 12:
                return "Gorekrak";
            case 13:
                return "Wrirkronak";
            case 14:
                return "Ghaktogar";
            case 15:
                return "Onzar";
            case 16:
                return "Wuktur";
            case 17:
                return "Zhikgakk";
            case 18:
                return "Grunzokh";
            case 19:
                return "Skargragar";
            case 20:
                return "Arbuttz";
            case 21:
                return "Arrgard";
            case 22:
                return "Badfragg";
            case 23:
                return "Bragrak";
            case 24:
                return "Gigabob";
            case 25:
                return "Biglug";
            case 26:
                return "Blaktoof";
            case 27:
                return "Bogga-bogga";
            case 28:
                return "Bolgrog";
            case 29:
                return "Brakka";
            case 30:
                return "Brug";
            case 31:
                return "Bullgarg";
            case 32:
                return "Buzzgob";
            case 33:
                return "Chatak";
            case 34:
                return "Dragnatz";
            case 35:
                return "Garaghak";
            case 36:
                return "Gargash";
            case 37:
                return "Garmek";
            case 38:
                return "Garshul";
            case 39:
                return "Garskrak";
            case 40:
                return "Gazbag";
            case 41:
                return "Grimgutz";
            case 42:
                return "Hruk";
            case 43:
                return "Judrog";
            case 44:
                return "Khorga";
            case 45:
                return "Murgor";
            case 46:
                return "Ugblitz";
            case 47:
                return "Ungskar";
            case 48:
                return "Vorgnot";
            case 49:
                return "Zoggit";
            default:
                return "Khobunak";
        }
    }

    /**
     * Randomly chooses a title for createGobName() and createOrkName().
     *
     * @return Returns the chosen title.
     */
    private static String generateTitle() {
        switch (Utilities.createRandomNumber(50)) {
            case 1:
                return "Kriegsbeil";
            case 2:
                return "Boom-Boom";
            case 3:
                return "da groze Vernichta";
            case 4:
                return "Leichenmacha";
            case 5:
                return "Stahlschmettera";
            case 6:
                return "Schmetterkopf";
            case 7:
                return "Arm-Reiza";
            case 8:
                return "Unheilsfaust";
            case 9:
                return "Sturmläufa";
            case 10:
                return "Hirnkocha";
            case 11:
                return "Zertrümmera";
            case 12:
                return "Fleischreiza";
            case 13:
                return "Bergessa";
            case 14:
                return "Zerstöra";
            case 15:
                return "da Kaputtmacha";
            case 16:
                return "da Irre";
            case 17:
                return "Kriegstreiber";
            case 18:
                return "Wundenreiza";
            case 19:
                return "Rekkcooka";
            case 20:
                return "da Groze";
            case 21:
                return "Blutzahn";
            case 22:
                return "Todbringa";
            case 23:
                return "Sägebiss";
            case 24:
                return "Nur-Ein-Auge";
            case 25:
                return "Schädelknakka";
            case 26:
                return "Narbenkiefa";
            case 27:
                return "Schlachta";
            case 28:
                return "Dakka";
            case 29:
                return "Zerdrücka";
            case 30:
                return "Krallenkiefa";
            case 31:
                return "Blutfaust";
            case 32:
                return "Menschenzhakka";
            case 33:
                return "Knochenknacka";
            case 34:
                return "Bauchschlitza";
            case 35:
                return "Kopfknakka";
            case 36:
                return "Blutkocha";
            case 37:
                return "Grimmschädel";
            case 38:
                return "Eisenzahn";
            case 39:
                return "Mek Dakka";
            case 40:
                return "Grotstampfa";
            case 41:
                return "Götterstampfa";
            case 42:
                return "Chaosbringa";
            case 43:
                return "da Ohne-Gesicht";
            case 44:
                return "Kopfmalmer";
            case 45:
                return "da Brenna";
            case 46:
                return "da groze Vabrenna";
            case 47:
                return "Schwarzzahn";
            case 48:
                return "Grimmzahn";
            case 49:
                return "Nakkenbrecha";
            default:
                return "Hirnplatza";
        }
    }

}
