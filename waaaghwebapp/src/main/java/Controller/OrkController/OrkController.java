/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.OrkController;

import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import org.primefaces.event.TabChangeEvent;
import org.primefaces.event.TabCloseEvent;

/**
 *
 * @author mirko.schulze
 */
@Named
@ViewScoped
public class OrkController implements Serializable {

    private ArrayList<Ork> orks;

    public ArrayList<Ork> getOrks() {
        return orks;
    }

    public void setOrks(ArrayList<Ork> orks) {
        this.orks = orks;
    }

    @PostConstruct
    public void orkControllerInit() {
        this.orks = new ArrayList<>();
        for (byte b = 0; b <= 5; b++) {
            this.orks.add(EntityGenerator.generateOrk());
        }
    }
    

}
