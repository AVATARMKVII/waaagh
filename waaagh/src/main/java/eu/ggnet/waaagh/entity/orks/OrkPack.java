package eu.ggnet.waaagh.entity.orks;

import eu.ggnet.waaagh.entity.Being;
import java.util.ArrayList;
import java.util.Arrays;
import eu.ggnet.waaagh.tools.Utilities;
import eu.ggnet.waaagh.interfaces.Presentable;
import java.util.List;

/**
 * Each OrkPack instance contains multiple Ork instances within an ArrayList.
 * Contains methods executed by multiple Ork instances simultaneously. Contains
 * attributes orks(ArrayList), leader(Ork) and members(int); offers methods
 * attackBeing(Being), attackOrkPack(OrkPack) and fightPackUntilDeath(OrkPack);
 * only fully initialized instances.
 *
 * @author mirko.schulze
 */
public class OrkPack implements Presentable {

    private final ArrayList<Ork> orks;
    private Ork leader;
    private int members;

    /**
     * Constructor to initialize an OrkPack instance in a minimal birth state.
     * Private modification to prevent an instantiation in a minimal birth
     * state.
     */
    private OrkPack() {
        this.orks = new ArrayList<>();
    }

    /**
     * Constructor to form an OrkPack instance out of Ork instances.
     *
     * @param ork The Ork instance that shall found this OrkPack.
     * @param orks The Ork instances that shall form this OrkPack.
     */
    public OrkPack(Ork ork, Ork... orks) {
        this();
        this.leader = ork;
        this.orks.add(ork);
        this.orks.addAll(Arrays.asList(orks));
        this.calculateMembers();
    }

    /**
     * Constructor to form an OrkPack instance out of an List of Ork instances.
     *
     * @param orkList The List that contains the instances of Ork.
     * @see tools.EntityGenerator#generateOrkPack()
     */
    public OrkPack(List<Ork> orkList) {
        this();
        this.orks.addAll(orkList);
        this.leader = orks.get(0);
        this.calculateMembers();
    }

    public ArrayList<Ork> getOrks() {
        return orks;
    }

    public Ork getLeader() {
        return leader;
    }

    public int getMembers() {
        return members;
    }

    public void setLeader(Ork leader) {
        this.leader = leader;
    }

    @Override
    public String toString() {
        return "OrkPack{" + "orks=" + orks + ", leader=" + leader + ", members=" + members + '}';
    }

    @Override
    public void present() {
        this.orks.forEach(o -> o.present());
    }

    /**
     * This instance of OrkPack overwhelms an instance of Being: every Ork in
     * this OrkPack calls attackAndCounter() upon an instance of Being.
     *
     * @param being The Being that is getting attacked by this OrkPack.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public void attackBeing(Being being, boolean useRandomizedHits) {
        this.orks.forEach(o -> {
            o.attackAndCounter(being, useRandomizedHits);
        });
    }

    /**
     * Two OrkPack instances repeat fight() until one is victorious.
     *
     * @param orkPack The OrkPack that is getting attacked by this OrkPack.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public void fightPackUntilDeath(OrkPack orkPack, boolean useRandomizedHits) {
        while (this.orks.isEmpty() != true) {
            this.attackOrkPack(orkPack, useRandomizedHits);
            if (!orkPack.getOrks().isEmpty()) {
                orkPack.attackOrkPack(this, useRandomizedHits);
            }
            if (this.orks.isEmpty() | orkPack.orks.isEmpty()) {
                break;
            }
        }
    }

    /**
     * Every Ork instance in this OrkPack starts an attack against a random Ork
     * instance in OrkPack orkPack. Ork instances with zero health are
     * considered dead and removed from their OrkPack.
     *
     * @param orkPack The OrkPack that is getting attacked by this OrkPack.
     * @param useRandomizedHits A boolean to disable random outcomes via false.
     */
    public void attackOrkPack(OrkPack orkPack, boolean useRandomizedHits) {
        for (int i = 0; i < this.orks.size(); i++) {
            Ork randomDefender = orkPack.orks.get(Utilities.createRandomIndex(orkPack.orks));
            this.orks.get(i).attack(randomDefender, useRandomizedHits);
            if (!randomDefender.isAlive()) {
                orkPack.orks.remove(randomDefender);
            }
            if (orkPack.orks.isEmpty()) {
                orkPack.leader = null;
                break;
            }
        }
    }

    /**
     * Iterates over the ArrayList orks in this OrkPack. Increases this
     * instances attribute members by one for each Ork in the list.
     */
    private void calculateMembers() {
        this.orks.forEach(o -> this.members++);
    }

}
