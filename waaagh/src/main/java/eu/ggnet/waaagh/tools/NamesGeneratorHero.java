package eu.ggnet.waaagh.tools;

import eu.ggnet.waaagh.entity.Hero;

/**
 * Contains switch-cases with five male names, five female names and five
 * titles; offers static methods to provide instances of Hero with a random
 * name.
 *
 * @author mirko.schulze
 */
public class NamesGeneratorHero {

    /**
     * Private constructor to prevent an instantiation.
     */
    private NamesGeneratorHero() {

    }

    /**
     * Combines the random Strings from generateName(Hero hero) and
     * generateGenderedTitle(Hero hero) into a name for a Hero.
     *
     * @param hero The Hero that shall be named.
     * @return Returns the created name.
     */
    public static String createName(Hero hero) {
        return generateName(hero) + " " + generateGenderedTitle(hero);
    }

    /**
     * Randomly chooses a name for createName(Hero hero), depending on the
     * Hero's gender.
     *
     * @param hero The Hero that shall be named.
     * @return Returns the chosen name.
     */
    private static String generateName(Hero hero) {
        switch (hero.getGender()) {
            case FEMALE:
                switch (Utilities.createRandomNumber(10)) {
                    case 1:
                        return "Anna";
                    case 2:
                        return "Klara";
                    case 3:
                        return "Isabelle";
                    case 4:
                        return "Hildegard";
                    default:
                        return "Marie";
                }
            default:
                switch (Utilities.createRandomNumber(10)) {
                    case 1:
                        return "Dieter";
                    case 2:
                        return "Olaf";
                    case 3:
                        return "Hans";
                    case 4:
                        return "Jan";
                    default:
                        return "Mirko";
                }
        }
    }

    /**
     * Alters the String from generateTitle(Hero hero) to match the Hero's
     * gender.
     *
     * @param hero The Hero that shall be named.
     * @return Returns the altered title.
     */
    private static String generateGenderedTitle(Hero hero) {
        switch (hero.getGender()) {
            case FEMALE:
                return hero.getGender().getPronoun() + " " + generateTitle() + "in";
            default:
                return hero.getGender().getPronoun() + " " + generateTitle();
        }
    }

    /**
     * Randomly chooses a title for createName(Hero hero).
     *
     * @return Returns the chosen title.
     */
    private static String generateTitle() {
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                return "Zerstörer";
            case 2:
                return "Ausweider";
            case 3:
                return "Goblinjäger";
            case 4:
                return "Auslöscher";
            default:
                return "Orkschlächter";
        }
    }

}
