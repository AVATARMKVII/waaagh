/*
*
* by Mirko Schulze
 */
package eu.ggnet.waaagh.entity;

import eu.ggnet.waaagh.tools.EntityGenerator;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class BeingTest {

    private Being attacker;
    private Being defender;

    public BeingTest() {
    }

    @Before
    public void setUp() {
        attacker = EntityGenerator.generateFemaleHero();
        attacker.setPower(5);
        attacker.setMaxHealth(10);
        attacker.setActHealth(attacker.getMaxHealth());
        attacker.setResistance(0);
        defender = EntityGenerator.generateFemaleHero();
        defender.setPower(5);
        defender.setMaxHealth(10);
        defender.setActHealth(defender.getMaxHealth());
        defender.setResistance(0);
    }

    /**
     * Test of isAlive method, of class Being.
     */
    @Test
    public void testIsAliveTrue() {
        assertThat(attacker.isAlive()).as("attacker should be alive").isTrue();
        attacker.attack(defender, false);
        assertThat(defender.isAlive()).as("defender should be alive").isTrue();
    }

    /**
     * Test of isAlive method, of class Being.
     */
    @Test
    public void testIsAliveFalse() {
        defender.setActHealth(1);
        attacker.attack(defender, false);
        assertThat(defender.isAlive()).as("defender should be dead").isFalse();
    }

    /**
     * Test of implementation of Combarable, in class Being. Attacker is
     * stronger that defender, new Being hero is as strong as attacker.
     */
    @Test
    public void testCompareTo() {
        Being hero = EntityGenerator.generateFemaleHero();
        hero.setPower(5);
        defender.setPower(1);
        assertThat(hero.compareTo(attacker)).as("equal power values should result in 0")
                .isStrictlyBetween(defender.compareTo(attacker), attacker.compareTo(defender)).isZero();
        assertThat(attacker.compareTo(defender)).as("should result in 1")
                .isGreaterThan(attacker.compareTo(hero))
                .isNotZero().isEqualTo(1);
        assertThat(defender.compareTo(attacker)).as("should result in -1")
                .isLessThan(attacker.compareTo(hero))
                .isNotZero().isEqualTo(-1);
    }

    /**
     * Test of attack method, of class Being.
     */
    @Test
    public void testAttack() {
        attacker.attack(defender, false);
        assertThat(defender.getActHealth()).as("defenders health should be 5").isEqualTo(5);
    }

    /**
     * Test of attackAndCounter method, of class Being.
     */
    @Test
    public void testAttackAndCounter() {
        attacker.attackAndCounter(defender, false);
        assertThat(defender.getActHealth()).as("defenders health should be 5").isEqualTo(5);
        assertThat(defender.isAlive()).as("defender should be alive").isTrue();
        assertThat(attacker.getActHealth()).as("attackers health should be 5").isEqualTo(5);
        assertThat(attacker.isAlive()).as("attacker should be alive").isTrue();
    }

    /**
     * Test of fightUntilDeath method, of class Being. Resistance set to zero to
     * ease maths.
     */
    @Test
    public void testFightUntilDeath() {
        attacker.fightUntilDeath(defender, false);
        assertThat(attacker.getActHealth()).as("attacker should survive with 5 health").isEqualTo(5);
        assertThat(attacker.isAlive()).as("attacker should be alive").isTrue();
        assertThat(defender.isAlive()).as("defender should NOT be alive").isFalse();
    }

}
