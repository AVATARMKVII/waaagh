package eu.ggnet.waaagh.entity;

import static eu.ggnet.waaagh.distinctions.Colours.ANSI_CYAN;

/**
 * Contains a static String to prefix messages "spoken" by the narrator.
 *
 * @author mirko.schulze
 */
public class Narrator {

    /**
     * Private constructor to prevent instantiating.
     */
    private Narrator() {

    }

    private static final String NARRATOR = "Erzähler: '" + ANSI_CYAN;

    /**
     *
     * @return Returns a static String including a prefix and an ANSI colour to
     * indicate the narrator in output messages.
     */
    public static String getNARRATOR() {
        return NARRATOR;
    }

}
