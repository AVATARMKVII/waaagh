package eu.ggnet.waaagh.rage.distinctions;

import eu.ggnet.waaagh.distinctions.Colours;

/**
 * Contains an enum to differentiate between various instances of RagingOrkPack.
 * Provides each RagingOrk with a colour and a symbol for visualization. Used
 * for The Rage.
 *
 * @author mirko.schulze
 */
public enum OrkSquads {

    MOSHAZ(Colours.ANSI_GREEN, 'M'),
    BALLAZ(Colours.ANSI_YELLOW, 'B'),
    KOMMANDOZ(Colours.ANSI_RED, 'K');

    private final String colour;
    private final char badge;

    private OrkSquads(String colour, char badge) {
        this.colour = colour;
        this.badge = badge;
    }

    public String getColour() {
        return colour;
    }

    public char getBadge() {
        return badge;
    }

}
