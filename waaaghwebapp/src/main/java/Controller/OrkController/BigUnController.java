/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller.OrkController;

import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.io.Serializable;
import java.util.ArrayList;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Mirko Schulze <mirko.schulze at gg-net.eu>
 */
@Named
@ViewScoped
public class BigUnController implements Serializable {

    private ArrayList<Ork> bigUns;

    public ArrayList<Ork> getBigUns() {
        return bigUns;
    }

    public void setBigUns(ArrayList<Ork> bigUns) {
        this.bigUns = bigUns;
    }

    @PostConstruct
    public void bigUnInit() {
        this.bigUns = new ArrayList<>();
        for (byte b = 0; b <= 5; b++) {
            this.bigUns.add(EntityGenerator.generateBigUn());
        }
    }

}
