package eu.ggnet.waaagh.tools;

import eu.ggnet.waaagh.distinctions.Genders;
import eu.ggnet.waaagh.entity.Hero;
import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.entity.orks.OrkCamp;
import eu.ggnet.waaagh.entity.orks.OrkPack;
import java.util.ArrayList;

/**
 * Contains static methods to instantiate different versions of different kinds
 * of Being.
 *
 * @author mirko.schulze
 */
public class EntityGenerator {

    /**
     * Returns a male instance of Hero.
     *
     * @return Returns a male instance of Hero.
     */
    public static Hero generateMaleHero() {
        return new Hero(Genders.MALE);
    }

    /**
     * Returns a female instance of Hero.
     *
     * @return Returns a female instance of Hero.
     */
    public static Hero generateFemaleHero() {
        return new Hero(Genders.FEMALE);
    }

    /**
     * Returns a weak instance of Ork.
     *
     * @return Returns a weak instance of Ork.
     */
    public static Ork generateGoblin() {
        return new Ork("Goblin",
                NamesGeneratorOrk.createGobName(),
                Utilities.createRandomNumber(5) + 3,
                Utilities.createRandomNumber(5) + 5,
                Utilities.createRandomNumber(3));
    }

    /**
     * Returns an instance of Ork.
     *
     * @return Returns an instance of Ork.
     */
    public static Ork generateOrk() {
        return new Ork("Ork",
                NamesGeneratorOrk.createOrkName(),
                Utilities.createRandomNumber(5) + 5,
                Utilities.createRandomNumber(5) + 10,
                Utilities.createRandomNumber(3) + 1);
    }

    /**
     * Returns a strong instance of Ork.
     *
     * @return Returns a strong instance of Ork.
     */
    public static Ork generateBlackOrk() {
        return new Ork("Schwarzork",
                NamesGeneratorOrk.createOrkName(),
                Utilities.createRandomNumber(10) + 7,
                Utilities.createRandomNumber(10) + 15,
                Utilities.createRandomNumber(3) + 2);
    }

    /**
     * Returns a boss instance of Ork.
     *
     * @return Returns a boss instance of Ork.
     */
    public static Ork generateBigUn() {
        return new Ork("Big Un'",
                NamesGeneratorOrk.createBozzName(),
                Utilities.createRandomNumber(5) + 10,
                Utilities.createRandomNumber(5) + 20,
                Utilities.createRandomNumber(5) + 3);
    }

    /**
     * Returns a strong boss instance of Ork.
     *
     * @return Returns a strong boss instance of Ork.
     */
    public static Ork generateWaaaghBozz() {
        return new Ork("Waaagh Bozz",
                NamesGeneratorOrk.createWaaaghBozzName(),
                Utilities.createRandomNumber(10) + 15,
                Utilities.createRandomNumber(10) + 25,
                Utilities.createRandomNumber(5) + 5);
    }

    /**
     * Returns an instance of OrkPack with a random amount of weak Ork instances
     * inside.
     *
     * @return Returns an instance of OrkPack with a random amount of weak Ork
     * instances inside.
     */
    public static OrkPack generateGobPack() {
        ArrayList<Ork> orks = new ArrayList<>();
        for (byte b = 0; b < Utilities.createRandomAmountOfMembers(); b++) {
            orks.add(EntityGenerator.generateGoblin());
        }
        return new OrkPack(orks);
    }

    /**
     * Returns an instance of OrkPack with a random amount of Ork instances
     * inside.
     *
     * @return Returns an instance of OrkPack with a random amount of Ork
     * instances inside.
     */
    public static OrkPack generateOrkPack() {
        ArrayList<Ork> orks = new ArrayList<>();
        orks.add(EntityGenerator.generateBlackOrk());
        for (byte b = 0; b < Utilities.createRandomAmountOfMembers(); b++) {
            orks.add(EntityGenerator.generateOrk());
        }
        return new OrkPack(orks);
    }

    /**
     * Returns an instance of OrkPack with a random amount of strong Ork
     * instances and a Bozz Ork inside.
     *
     * @return Returns an instance of OrkPack with a random amount of strong Ork
     * instances and a Bozz Ork inside.
     */
    public static OrkPack generateBlackOrkPack() {
        ArrayList<Ork> orks = new ArrayList<>();
        orks.add(EntityGenerator.generateBigUn());
        for (byte b = 0; b < Utilities.createRandomAmountOfMembers(); b++) {
            orks.add(EntityGenerator.generateBlackOrk());
        }
        return new OrkPack(orks);
    }

    /**
     * Returns an instance of OrkPack with a random amount of strong Bozz Ork
     * instances inside.
     *
     * @return Returns an instance of OrkPack with a random amount of strong
     * Bozz Ork instances inside.
     */
    public static OrkPack generateWaaaghGuard() {
        ArrayList<Ork> orks = new ArrayList<>();
        orks.add(EntityGenerator.generateWaaaghBozz());
        for (byte b = 0; b < Utilities.createRandomAmountOfMembers(); b++) {
            orks.add(EntityGenerator.generateBigUn());
        }
        return new OrkPack(orks);
    }

    /**
     * Returns an instance of OrkCamp with 3 instances of a GobPack inside.
     *
     * @return Returns an instance of OrkCamp with 3 instances of a GobPack
     * inside.
     */
    public static OrkCamp generateGobCamp() {
        OrkCamp oC = new OrkCamp();
        for (int i = 0; i < 3; i++) {
            oC.getPacks().add(EntityGenerator.generateGobPack());
        }
        return oC;
    }

    /**
     * Returns an instance of OrkCamp with 4 instances of a OrkPack inside.
     *
     * @return Returns an instance of OrkCamp with 3 instances of a GobPack
     * inside.
     */
    public static OrkCamp generateOrkCamp() {
        OrkCamp oC = new OrkCamp();
        for (int i = 0; i < 4; i++) {
            oC.getPacks().add(EntityGenerator.generateOrkPack());
        }
        return oC;
    }

    /**
     * Returns an instance of OrkCamp with 5 instances of a WaaaghGuardPack inside.
     *
     * @return Returns an instance of OrkCamp with 3 instances of a GobPack
     * inside.
     */
    public static OrkCamp generateWaaaghCamp() {
        OrkCamp oC = new OrkCamp();
        for (int i = 0; i < 5; i++) {
            oC.getPacks().add(EntityGenerator.generateWaaaghGuard());
        }
        return oC;
    }

}
