/*
Author: Mirko Schulze
 */
package eu.ggnet.waaagh.entity;

import eu.ggnet.waaagh.tools.EntityGenerator;
import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author mirko.schulze
 */
public class ExpTest {

    Hero hero;

    public ExpTest() {
    }

    @Before
    public void setUp() {
        hero = EntityGenerator.generateMaleHero();
    }

    /**
     * Test of gainExp method, of class Hero.
     */
    @Test
    public void testGainExp() {
        int boost = 7;
        hero.gainExp(boost);
        assertThat(hero.getExp()).as("hero exp should be " + boost).isEqualTo(boost).isEqualTo(hero.getAbsExp());
        hero.gainExp(boost);
        assertThat(hero.getExp()).as("hero exp should be " + (2 * boost)).isEqualTo(boost * 2).isEqualTo(hero.getAbsExp());
        hero.gainExp(boost);
        assertThat(hero.getExp()).as("hero exp should be " + (3 * boost)).isEqualTo(boost * 3).isEqualTo(hero.getAbsExp());
        assertThat(hero.getLvl()).as("hero lvl should be 1").isEqualTo(1);
        assertThat(hero.getLvlCap()).as("hero lvlCap should be 25").isEqualTo(25);
    }

    /**
     * Test of levelUp method, of class Hero.
     */
    @Test
    public void testMultipleLevelsUp() {
        int level = 0;
        int levelCap = 0;
        if (level < 10) {
            while (hero.getLvl() < 10) {
                level++;
                levelCap += 25;
                assertThat(hero.getLvl()).as("hero lvl should be " + level).isEqualTo(level);
                assertThat(hero.getLvlCap()).as("hero lvlCap should be " + levelCap).isEqualTo(levelCap);
                hero.gainExp(levelCap);
            }
        }
        if (hero.getLvl() == 10) {
            for (int i = 1; i <= 2; i++) {
                level++;
                levelCap += 50;
                assertThat(hero.getLvl()).as("hero lvl should be " + level).isEqualTo(level);
                assertThat(hero.getLvlCap()).as("hero lvlCap should be " + levelCap).isEqualTo(levelCap);
                hero.gainExp(levelCap);
            }
        }
    }
}
