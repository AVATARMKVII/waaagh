package eu.ggnet.waaagh.waaagh;

import eu.ggnet.waaagh.entity.orks.OrkPack;
import eu.ggnet.waaagh.tools.EntityGenerator;
import java.util.Collections;

/**
 * 
 *
 * @author mirko.schulze
 */
public class Waaagh {

    public static void main(String[] args) {

        OrkPack orkPack = EntityGenerator.generateOrkPack();
        orkPack.present();
        System.out.println("");
        System.out.println("");
        Collections.sort(orkPack.getOrks());
        orkPack.present();
        
        
    }
}
