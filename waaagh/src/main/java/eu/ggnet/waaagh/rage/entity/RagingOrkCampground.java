package eu.ggnet.waaagh.rage.entity;

import eu.ggnet.waaagh.distinctions.Colours;
import java.awt.Point;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import eu.ggnet.waaagh.rage.tools.RagingUtilities;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static
 *
 * @author mirko.schulze
 */
public class RagingOrkCampground {

    private final int maxWidth = 50;
    private final int minWidth = maxWidth - maxWidth;
    private final int centralXPosition = maxWidth / 2;
    private final int maxHeight = 8;
    private final int minHeight = maxHeight - maxHeight;
    private final int centralYPosition = maxHeight / 2;
    private final ArrayList<Point> positions = new ArrayList<>();

    public int getWidth() {
        return maxWidth;
    }

    public int getHeight() {
        return maxHeight;
    }

    public int getCentralXPosition() {
        return centralXPosition;
    }

    public int getCentralYPosition() {
        return centralYPosition;
    }

    public ArrayList<Point> getPositions() {
        return positions;
    }

    @Override
    public String toString() {
        return "OrkCampGround{" + "height=" + maxHeight + ", width=" + maxWidth + '}';
    }

    /**
     * Constructor to initialize an RagingOrkCampground instance.
     */
    public RagingOrkCampground() {
        for (int y = 0; y <= maxHeight; y++) {
            for (int x = 1; x <= maxWidth; x++) {
                positions.add(new Point(x, y));
            }
        }
    }

    /**
     * Displays a 2D representation of a RagingOrkCamp instance.
     *
     * @param ragingOrkCamp The RagingOrkCamp instance that shall be displayed.
     */
    public void displayPlayground(RagingOrkCamp ragingOrkCamp) {
        Point defaultPoint = new Point();
        for (int y = -1; y < maxHeight; y++) {
            defaultPoint.y = y;
            for (int x = 0; x < maxWidth; x++) {
                defaultPoint.x = x;
                printElements(defaultPoint, ragingOrkCamp);
            }
            System.out.println();
        }
        System.out.println();
        Utilities.sleepForASecond();
    }

    /**
     * Provides a RagingOrk instance with a random position on the
     * RagingOrkCampground.
     *
     * @param ragingOrk The RagingOrk instance that shall get a position.
     */
    public void submitAndDeletePositionOrk(RagingOrk ragingOrk) {
        Random R = new Random();
        int index = R.nextInt(getPositions().size());
        ragingOrk.setPosition(getPositions().get(index));
        getPositions().remove(index);
        if (ragingOrk.getPosition().x == minWidth) {
            ragingOrk.setXCoordinate(minWidth + 1);
        } else if (ragingOrk.getPosition().x == (maxWidth - 1)) {
            ragingOrk.setXCoordinate(maxWidth - 2);
        } else if (ragingOrk.getPosition().y == (minHeight)) {
            ragingOrk.setYCoordinate(minHeight + 1);
        } else if (ragingOrk.getPosition().y == maxHeight - 1) {
            ragingOrk.setYCoordinate(maxHeight - 2);
        }
    }

    /**
     * Provides each RagingOrk instance in an RagingOrkpack with a random
     * position on the agingOrkCampgound.
     *
     * @param ragingOrkPack The RagingOrkPack that contains the instances of
     * RagingOrk that shall get a position.
     */
    public void submitAndDeletePositionPack(RagingOrkPack ragingOrkPack) {
        ragingOrkPack.getRagingOrks().forEach(ro -> this.submitAndDeletePositionOrk(ro));
    }

    /**
     * Provides each RagingOrk instance in an RagingOrkPack in an RagingOrkCamp
     * with a random position on the RagingOrkCampground.
     *
     * @param ragingOrkCamp The RagingOrkCamp that contains the instances of
     * RagingOrk that shall get a position.
     */
    public void submitAndDeletePositionsCamp(RagingOrkCamp ragingOrkCamp) {
        ragingOrkCamp.getRagingPacks().forEach(rop -> this.submitAndDeletePositionPack(rop));
    }

    /**
     * This vararg calls moveToRandomPosition upon every submitted instance of
     * RagingOrkpack.
     *
     * @param ragingOrkPacks The RagingOrkPacks that contain the instances of
     * RagingOrk that shall get a position.
     */
    public void randomPackPositions(RagingOrkPack... ragingOrkPacks) {
        Arrays.asList(ragingOrkPacks).forEach(rop -> this.moveToRandomPosition(rop));
    }

    /**
     * Moves RagingOrk instances in a RagingOrkPack to random positions.
     *
     * @param orkPack The RagingOrkPack that contains the instances of RagingOrk
     * that shall get a position.
     */
    private void moveToRandomPosition(RagingOrkPack orkPack) {
        orkPack.getRagingOrks().forEach(o -> o.getPosition().setLocation(RagingUtilities.createRandomX(this), RagingUtilities.createRandomY(this)));
    }

    /**
     * Checks if a submitted Point object matches conditioned positions.
     *
     * @param point The submitted Point object.
     */
    private void printElements(Point point, RagingOrkCamp ragingOrkCamp) {
        if (printOrksTest(point, ragingOrkCamp) == true) {
            printOrks(point, ragingOrkCamp);
        } else if (printCampNameTest(point, ragingOrkCamp) == true) {
            printCampName(point, ragingOrkCamp);
        } else if (printWallTest(point) == true) {
            printWall(point);
        } else {
            System.out.print(' ');
        }
    }

    /**
     * Displays RagingOrk instances with their colour and badge.
     *
     * @param point The position that shall be checked.
     * @param ragingOrkCamp The instance of RagingOrkCamp that shall be
     * displayed.
     */
    private void printOrks(Point point, RagingOrkCamp ragingOrkCamp) {
        for (RagingOrkPack ragingOrkPack : ragingOrkCamp.getRagingPacks()) {
            for (RagingOrk ragingOrk : ragingOrkPack.getRagingOrks()) {
                if (point.equals(ragingOrk.getPosition())) {
                    System.out.print(ragingOrk.getSquad().getColour() + ragingOrk.getSquad().getBadge() + Colours.ANSI_RESET);
                }
            }
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of RagingOrk
     * instances.
     *
     * @param point The position that shall be checked.
     * @param ragingOrkCamp The instance of RagingOrkCamp that shall be
     * displayed.
     * @return Returns true if the submitted Point object equals to a Point
     * object marked as position of an RagingOrk instance.
     */
    private boolean printOrksTest(Point point, RagingOrkCamp ragingOrkCamp) {
        for (RagingOrkPack ragingOrkPack : ragingOrkCamp.getRagingPacks()) {
            for (RagingOrk ragingOrk : ragingOrkPack.getRagingOrks()) {
                if (point.equals(ragingOrk.getPosition())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Displays the name of the RagingOrkCamp above.
     *
     * @param point The position that shall be checked.
     * @param ragingOrkCamp The instance of RagingOrkCamp hat shall be
     * displayed.
     */
    private void printCampName(Point point, RagingOrkCamp ragingOrkCamp) {
        int pos = (maxWidth / 2) - (ragingOrkCamp.getName().length() / 2);
        for (int i = 0; i < ragingOrkCamp.getName().length(); i++) {
            if (point.x == pos && point.y == -1) {
                System.out.print(ragingOrkCamp.getName().toUpperCase().charAt(i));
            }
            pos++;
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of the
     * RagingOrkCamp's name.
     *
     * @param point The position that shall be checked.
     * @param ragingOrkCamp The instance of RagingOrkCamp that shall be
     * displayed.
     * @return Returns true if the submitted Point object equals to a Point
     * object in the area marked for the RagingOrkCamp's name.
     */
    private boolean printCampNameTest(Point point, RagingOrkCamp ragingOrkCamp) {
        int pos = (maxWidth / 2) - (ragingOrkCamp.getName().length() / 2);
        for (int i = -1; i < ragingOrkCamp.getName().length(); i++) {
            if (point.x == pos && point.y == -1) {
                return true;
            }
            pos++;
        }
        return false;
    }

    /**
     * Displays the surrounding fence.
     *
     * @param point The position that shall be checked.
     */
    private void printWall(Point point) {
        if ((point.x == (minWidth) || point.x == (maxWidth - 1)) && point.y >= 0) {
            System.out.print('|');
        } else if (point.y == (minHeight) || point.y == (maxHeight - 1)) {
            System.out.print('-');
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of the
     * surrounding fence.
     *
     * @param point The position that shall be checked.
     * @return Returns true if the submitted Point object equals to a Point
     * object in the area marked for fence.
     */
    private boolean printWallTest(Point point) {
        if ((point.x == (minWidth) || point.x == (maxWidth - 1)) && point.y >= 0) {
            return true;
        } else if (point.y == (minHeight) || point.y == (maxHeight - 1)) {
            return true;
        }
        return false;
    }

    /**
     * Moves RagingOrk instances in an RagingOrkPack to central positions inside
     * the RagingOrkCamp.
     *
     * @param row The line in which this instance of RagingOrkpack is
     * positioned.
     * @param ragingOrkPack The instance of RagingOrkPack that shall be moved.
     */
    public void moveToFightPosition(int row, RagingOrkPack ragingOrkPack) {
        int counter = 0;
        int xPosition = this.centralXPosition;
        for (RagingOrk ragingOrk : ragingOrkPack.getRagingOrks()) {
            if (counter % 2 == 0) {
                ragingOrk.getPosition().setLocation(xPosition + counter, row);
            } else if (counter % 2 != 0) {
                ragingOrk.getPosition().setLocation(xPosition - counter, row);
            }
            xPosition = ragingOrkPack.getRagingOrks().get(counter).getPosition().x;
            counter++;
        }
    }

    /**
     * Moves RagingOrk instances in an RagingOrkPack to positions on the edge of
     * the RagingOrkCamp.
     *
     * @param ragingOrkPack The instance of RagingOrkPack that shall be moved.
     */
    public void moveToWitnessPosition(RagingOrkPack ragingOrkPack) {
        int counter1 = 0;
        int counter2 = 0;
        int xPosition = this.getCentralXPosition();
        for (RagingOrk ragingOrk : ragingOrkPack.getRagingOrks()) {
            if (Utilities.createRandomNumber(10) <= 5) {
                if (counter1 % 2 == 0) {
                    ragingOrk.getPosition().setLocation(xPosition + counter1, 2);
                } else if (counter1 % 2 != 0) {
                    ragingOrk.getPosition().setLocation(xPosition - counter1, 2);
                }
                xPosition = ragingOrkPack.getRagingOrks().get(counter1).getPosition().x;
                counter1++;
            } else {
                if (counter2 % 2 == 0) {
                    ragingOrk.getPosition().setLocation(xPosition + counter2, 5);
                } else if (counter2 % 2 != 0) {
                    ragingOrk.getPosition().setLocation(xPosition - counter2, 5);
                }
                xPosition = ragingOrkPack.getRagingOrks().get(counter2).getPosition().x;
                counter2++;
            }

        }
    }

    /**
     * Moves the OrkPack instances in this OrkCamp instance to specified
     * positions.
     *
     * @param attackerPack The attacking RagingOrkPack , determined by the
     * outcome of act I.
     * @param witnessPack The bystanding RagingOrkPack, determined by the
     * outcome of act I.
     * @param ragingOrkCamp The RagingOrkCamp in which the RagingOrkPacks shall
     * be moved to their position.
     */
    public void prepareCombat(RagingOrkPack attackerPack, RagingOrkPack witnessPack, RagingOrkCamp ragingOrkCamp) {
        this.moveToFightPosition(this.getCentralYPosition(), attackerPack);
        this.moveToFightPosition(this.getCentralYPosition() - 1, ragingOrkCamp.getKommandoz());
        this.moveToWitnessPosition(witnessPack);
        this.displayPlayground(ragingOrkCamp);
    }

    /**
     * Moves the remaining OrkPack instances in this OrkCamp instance
     * face-to-face.
     *
     * @param ragingOrkPack The surviving RagingOrkPack, determined by the
     * outcome of act II.
     * @param ragingOrkCamp The RagingOrkCamp in which the RagingOrkPacks shall
     * be moved to their position.
     */
    public void prepareFinalCombat(RagingOrkPack ragingOrkPack, RagingOrkCamp ragingOrkCamp) {
        this.moveToFightPosition(this.centralYPosition, ragingOrkCamp.getKommandoz());
        this.moveToFightPosition(this.centralYPosition - 1, ragingOrkPack);
        this.displayPlayground(ragingOrkCamp);
    }

}
