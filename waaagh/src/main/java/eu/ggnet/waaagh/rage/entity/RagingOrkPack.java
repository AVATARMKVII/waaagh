package eu.ggnet.waaagh.rage.entity;

import eu.ggnet.waaagh.rage.distinctions.OrkSquads;
import java.util.ArrayList;
import eu.ggnet.waaagh.rage.output.OutputRagingOrk;
import eu.ggnet.waaagh.rage.output.OutputRagingOrkLeader;
import eu.ggnet.waaagh.rage.tools.RagingUtilities;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * RagingOrkPack extends OrkPack: covers attribute leader(OrkLeader) by
 * leader(RagingOrkLeader); offers method escalate; only fully initialized
 * instances: used for The Rage.
 *
 * @author mirko.schulze
 */
public class RagingOrkPack {

    private final ArrayList<RagingOrk> ragingOrks;
    private RagingOrkLeader ragingLeader;

    /**
     * Constructor to initialize RagingOrkPack in a minimal birth state.
     */
    public RagingOrkPack() {
        this.ragingOrks = new ArrayList<>();
    }

    /**
     * Constructor to fully initialize an RagingOrkPack instance.
     *
     * @param squad The visualization of the Ork instances in this instance
     * depends on sort.
     */
    public RagingOrkPack(OrkSquads squad) {
        this();
        this.ragingLeader = new RagingOrkLeader(squad);
        this.ragingOrks.add(this.ragingLeader);
        for (int i = 1; i < this.ragingLeader.getCharisma(); i++) {
            this.ragingOrks.add(new RagingOrk(this.ragingLeader));
        }
    }

    public ArrayList<RagingOrk> getRagingOrks() {
        return ragingOrks;
    }

    public RagingOrkLeader getRagingLeader() {
        return ragingLeader;
    }

    @Override
    public String toString() {
        return "RagingOrkPack{" + "ragingOrks=" + ragingOrks + ", ragingLeader=" + ragingLeader + '}';
    }

    /**
     * Selects a random RagingOrk in this RagingOrkPack instance to get picked
     * on. The RagingOrkLeader of this RagingOrkPack instance increases his
     * anger.
     */
    public void escalate() {
        OutputRagingOrk.bePissed(RagingUtilities.chooseRandomRagingOrk(this));
        this.ragingLeader.getAngry();
        if (this.ragingLeader.isAngry()) {
            OutputRagingOrkLeader.callForViolence(this.ragingLeader);
        }
    }

    /**
     * Two instances of RagingOrkPack repeat fight(RagingOrkPack) by turns.
     *
     * @param ragingOrkPack The attacked RagingOrkPack.
     */
    public void battle(RagingOrkPack ragingOrkPack) {
        this.fight(ragingOrkPack);
        if (!ragingOrkPack.getRagingOrks().isEmpty()) {
            ragingOrkPack.fight(this);
        }
    }

    /**
     * Every RagingOrk instance in this OrkPack starts an attack against a
     * random RagingOrk instance in the attacked RagingOrkPack. RagingOrk
     * instances with zero health are considered dead and removed from their
     * RagingOrkPack.
     *
     * @param ragingOrkPack The attacked RagingOrkPack.
     */
    private void fight(RagingOrkPack ragingOrkPack) {
        for (int i = 0; i < this.ragingOrks.size(); i++) {
            RagingOrk randomDefender = ragingOrkPack.ragingOrks.get(Utilities.createRandomIndex(ragingOrkPack.ragingOrks));
            this.ragingOrks.get(i).attack(randomDefender, true);
            if (!randomDefender.isAlive()) {
                OutputRagingOrk.die(randomDefender);
                ragingOrkPack.ragingOrks.remove(randomDefender);
            }
            if (ragingOrkPack.ragingOrks.isEmpty()) {
                break;
            }
        }

    }

}
