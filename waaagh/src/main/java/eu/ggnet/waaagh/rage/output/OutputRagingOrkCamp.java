package eu.ggnet.waaagh.rage.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.rage.entity.RagingOrk;
import eu.ggnet.waaagh.rage.entity.RagingOrkCamp;
import eu.ggnet.waaagh.rage.entity.RagingOrkLeader;
import eu.ggnet.waaagh.rage.tools.RagingUtilities;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains a static method to stream messages "spoken" by randomly chosen RagingOrks
 * inside an OrkCamp. Used for The Rage.
 *
 * @author mirko.schulze
 */
public class OutputRagingOrkCamp {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputRagingOrkCamp() {

    }

    /**
     * Displays a few messages to indicate unity belong the (surviving) OrkPack
     * instances in an OrkCamp instance.
     *
     * @param ragingOrkCamp The RaagingOrkCamp that contains the participating
     * Orks.
     */
    public static void niceCanon(RagingOrkCamp ragingOrkCamp) {
        RagingOrkLeader randomLeader = RagingUtilities.chooseRandomRagingLeader(ragingOrkCamp);
        System.out.println(randomLeader.getName() + ": " + randomLeader.getSquad().getColour() + "'Wir lieb'n dat Pl\u00fcndarn, Fern \u00fcba'm weiten Meer, Und T\u00f6t'n und M\u00f6rdarn, Und K\u00e4mpfn und noch mehr!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        RagingOrk randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkCamp);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Falla-riiiiiiiii! Falla-raaaaaaaa! Falla-riiiiiiiii! Falla-raaaaaaaa! So weit is' alles klar?'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomLeader = RagingUtilities.chooseRandomRagingLeader(ragingOrkCamp);
        System.out.println(randomLeader.getName() + ": " + randomLeader.getSquad().getColour() + "'Wir lieb'n dat Pl\u00fcndarn, Tief drinnen im Berch, Denn wenn wa komm' zum Pr\u00fcgeln, Vasteckt sich jeda Zwerch!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = RagingUtilities.chooseRandomRagingOrk(ragingOrkCamp);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSquad().getColour() + "'Falla-riiiiiiiii! Falla-raaaaaaaa! Falla-riiiiiiiii! Falla-raaaaaaaa! So weit is' alles klar?'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
