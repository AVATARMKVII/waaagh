package eu.ggnet.waaagh.entity;

import eu.ggnet.waaagh.distinctions.Genders;
import eu.ggnet.waaagh.distinctions.SortsOfBeings;
import eu.ggnet.waaagh.output.OutputBeing;
import eu.ggnet.waaagh.output.OutputHero;
import eu.ggnet.waaagh.output.OutputNarrator;
import eu.ggnet.waaagh.tools.NamesGeneratorHero;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Hero extends Being: extends Being by attributes gender(Genders), level(int),
 * exp(int), absExp(int) and levelCap(int); offers methods gainExp(int) and
 * levelUp(); only fully initialized instances.
 *
 * @author mirko.schulze
 */
public class Hero extends Being {

    private final Genders gender;
    private int lvl;
    private int exp;
    private int absExp;
    private int lvlCap;

    /**
     * Constructor to fully initialize an instance of Hero.
     *
     * @param gender The name of this instance depends on the gender.
     * @see tools.EntityGenerator#generateMaleHero()
     */
    public Hero(Genders gender) {
        super(SortsOfBeings.HERO);
        super.setPower(4);
        super.setMaxHealth(12);
        super.setActHealth(super.getMaxHealth());
        super.setResistance(2);
        this.gender = gender;
        super.setName(NamesGeneratorHero.createName(this));
        this.lvl = 1;
        this.exp = 0;
        this.absExp = this.exp;
        this.lvlCap = 25;
    }

    public Genders getGender() {
        return gender;
    }

    public int getLvl() {
        return lvl;
    }

    public int getExp() {
        return exp;
    }

    public int getAbsExp() {
        return absExp;
    }

    public int getLvlCap() {
        return lvlCap;
    }

    @Override
    public String toString() {
        return super.toString() + "Hero{" + "gender=" + gender
                + ", level=" + lvl + ", exp=" + exp + ", absExp="
                + absExp + ", levelCap=" + lvlCap + '}';
    }

    @Override
    public void attack(Being being, boolean useRandomizedHits) {
        if (useRandomizedHits) {
            if (Utilities.hitOrMiss()) {
                OutputHero.hit(this);
                being.setActHealth(being.getActHealth() - (this.getPower() - being.getResistance()));
                if (being.getActHealth() <= 0) {
                    OutputBeing.die(being);
                    being.setAlive(false);
                } else {
                    OutputBeing.getAttacked(being);
                }
            } else {
                OutputHero.miss(this);
            }
        } else {
            OutputHero.hit(this);
            being.setActHealth(being.getActHealth() - (this.getPower() - being.getResistance()));
            if (being.getActHealth() <= 0) {
                OutputBeing.die(being);
                being.setAlive(false);
            } else {
                OutputBeing.getAttacked(being);
            }
        }
    }

    @Override
    public void present() {
        System.out.println("Name = " + this.getName() + ", Kraft = " + this.getPower()
                + ", Gesundheit = " + this.getActHealth() + "/" + this.getMaxHealth()
                + ", Rüstung = " + this.getResistance() + ", Level = " + this.getLvl()
                + ", Ehrfarungspunkte bis zur nächten Stufe = " + (this.lvlCap - this.exp)
                + ", Ehrfarungspunkte insgesamt = " + this.absExp);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * The Hero's experience is increased by the submitted value. If the value
     * of exp reaches the value of levelCap, this Hero instance calls levelUp().
     *
     * @param exp The amount of experience this instance of Hero is granted.
     */
    public void gainExp(int exp) {
        this.exp += exp;
        this.absExp += exp;
        OutputNarrator.heroGainsExp(this, exp);
        if (this.exp >= this.lvlCap) {
            this.levelUp();
        }
    }

    /**
     * Increases this Hero instances level, power and health. Resets this Hero
     * instance's experience to zero. Increases the levelCap.
     */
    private void levelUp() {
        this.lvl++;
        this.setPower(this.getPower() + 2);
        this.setMaxHealth(this.getMaxHealth() + 5);
        this.setActHealth(this.getMaxHealth());
        this.exp -= this.lvlCap;
        if (this.lvl < 10) {
            this.lvlCap += 25;
        } else if (this.lvl < 20) {
            this.lvlCap += 50;
        }
        OutputNarrator.heroLevelsUp(this);
    }

}
