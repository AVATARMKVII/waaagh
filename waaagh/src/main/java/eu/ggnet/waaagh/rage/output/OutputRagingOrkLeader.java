package eu.ggnet.waaagh.rage.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.rage.entity.RagingOrkLeader;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by the RagingOrkLeader of
 * a RagingOrkPack. Used for The Rage.
 *
 * @author mirko.schulze
 */
public class OutputRagingOrkLeader {

    /**
     * Displays a few messages to indicate an RagingOrkLeader instance in an
     * RagingOrkPack instance reaching it's anger-limit.
     *
     * @param ragingOrkLeader The RagingOrkLeader of the participating
     * RagingOrkPack.
     */
    public static void callForViolence(RagingOrkLeader ragingOrkLeader) {
        System.out.println(ragingOrkLeader.getName() + ": " + ragingOrkLeader.getSquad().getColour() + "'...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(ragingOrkLeader.getName() + ": " + ragingOrkLeader.getSquad().getColour() + "'Jetzt reicht's!!Los Jungz!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(ragingOrkLeader.getName() + ": " + ragingOrkLeader.getSquad().getColour() + "'Die M@7t3rF*\u00a7K# mosh'n wa' jetzt wech!!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate the victory in a fight scene.
     *
     * @param ragingOrkLeader The RagingOrkLeader instance of the participating
     * RagingOrkPack.
     */
    public static void claimVictory(RagingOrkLeader ragingOrkLeader) {
        System.out.println(ragingOrkLeader.getName() + ": " + ragingOrkLeader.getSquad().getColour() + "'HAHA!!Stampft'se!Stampft'se bis nix mehr \u00fcbrich is!!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
