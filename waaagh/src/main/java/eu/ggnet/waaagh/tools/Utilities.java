package eu.ggnet.waaagh.tools;

import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.entity.orks.OrkCamp;
import eu.ggnet.waaagh.entity.orks.OrkCampground;
import eu.ggnet.waaagh.entity.orks.OrkPack;
import java.util.List;
import java.util.Random;
import eu.ggnet.waaagh.output.ErrorMessage;

/**
 * Offers static methods for general purpose.
 *
 * @author mirko.schulze
 */
public class Utilities {

    /**
     * Private constructor to prevent an instantiation.
     */
    private Utilities() {

    }

    /**
     * Puts the thread to sleep for a specified time, an exception results in an
     * error message.
     *
     * @param time The waiting period in milliseconds.
     */
    public static void sleep(int time) {
        try {
//            throw new RuntimeException();
            Thread.sleep(time);
        } catch (InterruptedException e) {
            ErrorMessage.errorMessage("Ey, Bozz! Was'n da los?!?", "Sleep has thrown an exception.");
        }
    }

    /**
     * Puts the thread to sleep for a one second, an exception results in an
     * error message.
     *
     */
    public static void sleepForASecond() {
        try {
//            throw new RuntimeException();
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            ErrorMessage.errorMessage("Ey, Bozz! Was'n da los?!?", "Sleep has thrown an exception.");
        }
    }

    /**
     * Creates a random number between one and ten. Returns true if the created
     * number is below ten.
     *
     * @return Returns true if the created number is below ten.
     */
    public static boolean hitOrMiss() {
        return Utilities.createRandomNumber(10) <= 9;
    }

    /**
     * Creates a random number between one and the submitted limit.
     *
     * @param limit The inclusive top limit.
     * @return Returns the created number.
     */
    public static int createRandomNumber(int limit) {
        final Random R = new Random();
        return (R.nextInt(limit)) + 1;
    }

    /**
     * Creates a random number between zero and the size of the submitted list
     * to get a random index.
     *
     * @param list The List that shall be accessed.
     * @return Returns the created number.
     */
    public static int createRandomIndex(List list) {
        return Math.min(createRandomNumber(list.size()), list.size() <= 0 ? 0 : list.size() - 1);
    }

    /**
     * Creates a random number between five and ten to determine the size of an
     * OrkPack instance.
     *
     * @return Returns the created number.
     */
    public static int createRandomAmountOfMembers() {
        return createRandomNumber(10) + 5;
    }

    /**
     * Creates a number in the bounds of the size of an instance of OrkCamp to
     * set the x coordinate for a Point object.
     *
     * @param campground The OrkCampground on which a coordinate is needed.
     * @return Returns the created number.
     */
    public static int createRandomX(OrkCampground campground) {
        int min = 0;
        int max = campground.getMaxWidth();
        int random = createRandomNumber(max);
        if (random == (min | min + 1)) {
            return random + 2;
        } else if (random == (max | max - 1)) {
            return random - 2;
        } else {
            return random;
        }
    }

    /**
     * Creates a number in the bounds of the size of an instance of OrkCamp to
     * set the y coordinate for a Point object.
     *
     * @param campground The OrkCampground on which a coordinate is needed.
     * @return Returns the created number.
     */
    public static int createRandomY(OrkCampground campground) {
        int min = 0;
        int max = campground.getMaxHeight();
        int random = createRandomNumber(max);
        if (random == min) {
            return random + 1;
        } else if (random == max) {
            return random - 2;
        } else {
            return random;
        }
    }

    /**
     * Selects a random Ork instance in an OrkPack instance.
     *
     * @param orkPack The OrkPack in which the Ork is in.
     * @return Returns the chosen Ork.
     */
    public static Ork chooseRandomOrk(OrkPack orkPack) {
        return orkPack.getOrks().get(Utilities.createRandomIndex(orkPack.getOrks()));
    }

    /**
     * Selects a random Ork instance in an OrkCamp instance.
     *
     * @param orkCamp The OrkCamp in which the Ork is in.
     * @return Returns the chosen Ork.
     */
    public static Ork chooseRandomOrk(OrkCamp orkCamp) {
        OrkPack randomPack = orkCamp.getPacks().get(Utilities.createRandomIndex(orkCamp.getPacks()));
        return chooseRandomOrk(randomPack);
    }

}
