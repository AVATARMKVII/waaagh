package eu.ggnet.waaagh.distinctions;

/**
 * Contains an enum to differentiate between male and female instances of Hero.
 *
 * @author mirko.schulze
 */
public enum Genders {

    MALE("der"),
    FEMALE("die");

    private final String pronoun;

    private Genders(String pronoun) {
        this.pronoun = pronoun;
    }

    public String getPronoun() {
        return pronoun;
    }

}
