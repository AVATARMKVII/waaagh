package eu.ggnet.waaagh.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.entity.orks.Ork;
import eu.ggnet.waaagh.entity.orks.OrkCamp;
import eu.ggnet.waaagh.entity.orks.OrkPack;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains a static method to stream messages "spoken" by randomly chosen Orks
 * inside an OrkPack.
 *
 * @author mirko.schulze
 */
public class OutputOrkPack {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputOrkPack() {

    }

    /**
     * Displays a few messages to indicate a paean preceding a raid.
     *
     * @param orkPack The OrkPack that contains the participating instances of
     * Ork.
     */
    public static void niceCanon(OrkCamp orkPack) {
        Ork randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Wir lieb'n dat Pl\u00fcndarn, Fern \u00fcba'm weiten Meer, Und T\u00f6t'n und M\u00f6rdarn, Und K\u00e4mpfn und noch mehr!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Falla-riiiiiiiii! Falla-raaaaaaaa! Falla-riiiiiiiii! Falla-raaaaaaaa! So weit is' alles klar?'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Wir lieb'n dat Pl\u00fcndarn, Tief drinnen im Berch, Denn wenn wa komm' zum Pr\u00fcgeln, Vasteckt sich jeda Zwerch!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Falla-riiiiiiiii! Falla-raaaaaaaa! Falla-riiiiiiiii! Falla-raaaaaaaa! So weit is' alles klar?'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a few messages to indicate a paean following a raid.
     *
     * @param orkPack The OrkPack that contains the participating instances of
     * Ork.
     */
    public static void anthem(OrkPack orkPack) {
        Ork randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Wir sind da gr\u00f6\u00dften und da st\u00e4rksten!Denn wir sin' da Orkz!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Jeder wei\u00df dat gr\u00fcn am besten is'!Denn wir sin' da Orkz!" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Jeda wird von da Angst gesch\u00fcttelt!Denn wir sin' da Orkz!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        randomOrk = Utilities.chooseRandomOrk(orkPack);
        System.out.println(randomOrk.getName() + ": " + randomOrk.getSort().getColour() + "'Und wenn wir auf'm Weg sin' h\u00e4lt uns nix auf!Denn wir sin' da Orkz!!!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
