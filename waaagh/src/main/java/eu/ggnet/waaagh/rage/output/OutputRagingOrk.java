package eu.ggnet.waaagh.rage.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.output.OutputNarrator;
import eu.ggnet.waaagh.rage.entity.RagingOrk;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by a RagingOrk. Used for The Rage.
 *
 * @author mirko.schulze
 */
public class OutputRagingOrk {

    private OutputRagingOrk() {

    }

    /**
     * Displays a message to indicate a RagingOrk instance picking on another one.
     *
     * @param ragingOrk The RagingOrk that picks on another RagingOrk.
     */
    public static void beBored(RagingOrk ragingOrk) {
        String output1;
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                output1 = "'Is' dat langweilich hier...Eyy, du Orkfurz, ";
                break;
            case 2:
                output1 = "'Was'n das hier, kann man denn nich' ma'...Du Made, ";
                break;
            case 3:
                output1 = "'Wolken...Wolken...Wolken...Du da!Kleine Grünhaut, ";
                break;
            case 4:
                output1 = "'Wann geh'n wir denn endlich ma' wieda plündarn...Menschen-Streichla, ";
                break;
            default:
                output1 = "'Noch so'n lahmer Tach und ich dreh' durch...Hey, Frischfleich, ";
        }
        String output2;
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                output2 = "Zeit meine Stampfaz zu testen, h\u00f6h\u00f6h\u00f6!'";
                break;
            case 2:
                output2 = "lauf ma' lieber zu deiner Elfenmami!'";
                break;
            case 3:
                output2 = "geh' doch lieber mit deinen Grotz spielen!'";
                break;
            case 4:
                output2 = "der Spalta is' doch viel zu groz für dich, harharhar!!";
                break;
            default:
                output2 = "komm her, dann tu ich dir die Z\u00e4hne rausmach'n!'";
        }
        System.out.println(ragingOrk.getName() + ": " + ragingOrk.getSquad().getColour() + output1 + output2 + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a RagingOrk instance that got picked on.
     *
     * @param ragingOrk The RagingOrk that is getting picked on.
     */
    public static void bePissed(RagingOrk ragingOrk) {
        String output;
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                output = "'Lass das, du Mensch!'";
                break;
            case 2:
                output = "'Griffel weg, Grot-Kuschler!";
                break;
            case 3:
                output = "'Such' dir wen ander'n, Sackgesicht!";
                break;
            case 4:
                output = "'Schnauze'";
                break;
            default:
                output = "'Du Arsch-Ork!'";
        }
        System.out.println(ragingOrk.getName() + ": " + ragingOrk.getSquad().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a succesful attack.
     *
     * @param ragingOrk The attacking RagingOrk.
     */
    public static void hit(RagingOrk ragingOrk) {
        String output;
        switch (Utilities.createRandomNumber(10)) {
            case 1:
                output = "'Den Arm brauchst'e doch eh nicht mehr!!'";
                break;
            case 2:
                output = "'Immer mitten in die Fresse rein!!'";
                break;
            case 3:
                output = "'Plitsch!Platsch!'";
                break;
            case 4:
                output = "'Flieg, kleines Köpfchen!Flieg!!'";
                break;
            case 5:
                output = "'Dakka!Dakka!Dakka!!'";
                break;
            case 6:
                output = "'Friss das!'";
                break;
            case 7:
                output = "'HACKEN!HACKEN!!'";
                break;
            case 8:
                output = "'Plündarn!Mördarn!'";
                break;
            case 9:
                output = "'Treffa!Haha!!'";
                break;
            default:
                output = "'Mosh'n!Mosh'n!MOSHEN!!'";
        }
        System.out.println(ragingOrk.getName() + ": " + ragingOrk.getSquad().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a failing attack.
     *
     * @param ragingOrk The attacking RagingOrk.
     */
    public static void miss(RagingOrk ragingOrk) {
        String output;
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                output = "'Drookkz! Daneben!!'";
                break;
            case 2:
                output = "'Nu' halt doch endlich still, Grot!'";
                break;
            case 3:
                output = "'Graah, schon wieder mit den Augen zu..'";
                break;
            case 4:
                output = "'Huch?!'";
                break;
            default:
                output = "'Verdammt!Abgerutscht!!'";
        }
        System.out.println(ragingOrk.getName() + ": " + ragingOrk.getSquad().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        OutputNarrator.heMissed(ragingOrk);
        Utilities.sleepForASecond();
    }

    /**
     * Displays a message to indicate an instance of RagingOrk is dying.
     *
     * @param ragingOrk The dying RagingOrk.
     */
    public static void die(RagingOrk ragingOrk) {
        String output;
        switch (Utilities.createRandomNumber(10)) {
            case 1:
                output = "'Uuurrkss...'";
                break;
            case 2:
                output = "'AAaargghh..'";
                break;
            case 3:
                output = "'Gllubb...Bllluugg..'";
                break;
            case 4:
                output = "'Nein, nicht du verdammter..Hund...'";
                break;
            case 5:
                output = "'Gork...ruft mich zu sich...'";
                break;
            case 6:
                output = "'Ich komm' zu dir....Mork'";
                break;
            case 7:
                output = "'Das wars...für mich...'";
                break;
            case 8:
                output = "''Röchel!Gurgel!Splurtz!'";
                break;
            case 9:
                output = "'Dafür...wirst du bezahlen..'";
                break;
            default:
                output = "'.....'";
        }
        System.out.println(ragingOrk.getName() + ": " + ragingOrk.getSquad().getColour() + output + Colours.ANSI_RESET);
        OutputNarrator.heIsDead(ragingOrk);
        Utilities.sleepForASecond();
    }
}
