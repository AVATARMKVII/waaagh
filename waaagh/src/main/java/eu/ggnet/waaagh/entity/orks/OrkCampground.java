package eu.ggnet.waaagh.entity.orks;

import java.awt.Point;
import java.util.ArrayList;
import eu.ggnet.waaagh.distinctions.Colours;
import java.util.Arrays;
import java.util.Random;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains variables and methods to display a 2D representation of an OrkCamp
 * instance.
 *
 * @author mirko.schulze
 */
public class OrkCampground {

    private final int maxWidth = 50;
    private final int minWidth = 0;
    private final int centralXPosition = maxWidth / 2;
    private final int maxHeight = 8;
    private final int minHeight = 0;
    private final int centralYPosition = maxHeight / 2;
    private final ArrayList<Point> positions = new ArrayList<>();

    public int getMaxWidth() {
        return maxWidth;
    }

    public int getMinWidth() {
        return minWidth;
    }

    public int getCentralXPosition() {
        return centralXPosition;
    }

    public int getMaxHeight() {
        return maxHeight;
    }

    public int getMinHeight() {
        return minHeight;
    }

    public int getCentralYPosition() {
        return centralYPosition;
    }

    public ArrayList<Point> getPositions() {
        return positions;
    }

    @Override
    public String toString() {
        return "OrkCampGround{" + "height=" + maxHeight + ", width=" + maxWidth + '}';
    }

    /**
     * Constructor to initialize an OrkCampground instance.
     */
    public OrkCampground() {
        for (int y = 1; y < maxHeight; y++) {
            for (int x = 1; x < maxWidth; x++) {
                positions.add(new Point(x, y));
            }
        }
    }

    /**
     * Displays a 2D representation of an OrkCamp instance.
     *
     * @param orkCamp The OrkCamp instance that shall be displayed.
     */
    public void displayPlayground(OrkCamp orkCamp) {
        Point defaultPoint = new Point();
        for (int y = -1; y < maxHeight; y++) {
            defaultPoint.y = y;
            for (int x = 0; x < maxWidth; x++) {
                defaultPoint.x = x;
                printElements(defaultPoint, orkCamp);
            }
            System.out.println();
        }
        System.out.println();
        Utilities.sleepForASecond();
    }

    /**
     * Provides an Ork instance with a random position on the OrkCampground.
     *
     * @param ork The Ork instance that shall get a position.
     */
    public void submitAndDeletePositionOrk(Ork ork) {
        Random R = new Random();
        int index = R.nextInt(this.getPositions().size());
        ork.setPosition(this.getPositions().get(index));
        this.getPositions().remove(index);
    }

    /**
     * Provides each Ork instance in an Orkpack with a random position on the
     * OrkCampgound.
     *
     * @param orkPack The OrkPack that contains the instances of Ork that shall
     * get a position.
     */
    public void submitAndDeletePositionPack(OrkPack orkPack) {
        orkPack.getOrks().forEach(o -> this.submitAndDeletePositionOrk(o));
    }

    /**
     * Provides each Ork instance in an OrkPack in an OrkCamp with a random
     * position on the OrkCampground.
     *
     * @param orkCamp The OrkCamp that contains the instances of Ork that shall
     * get a position.
     */
    public void submitAndDeletePositionsCamp(OrkCamp orkCamp) {
        orkCamp.getPacks().forEach(op -> this.submitAndDeletePositionPack(op));
    }

    /**
     * This vararg calls moveToRandomPosition upon every submitted instance of
     * Orkpack.
     *
     * @param orkPacks The OrkPacks that contain the instances of Ork that shall
     * get a position.
     */
    public void randomPackPositions(OrkPack... orkPacks) {
        Arrays.asList(orkPacks).forEach(op -> this.moveToRandomPosition(op));
    }

    /**
     * Moves Ork instances in an OrkPack instance to random positions.
     *
     * @param orkPack The OrkPack that contains the instances of Ork that shall
     * get a position.
     */
    private void moveToRandomPosition(OrkPack orkPack) {
        orkPack.getOrks().forEach(o -> o.getPosition().setLocation(Utilities.createRandomX(this), Utilities.createRandomY(this)));
    }

    /**
     * Checks if a submitted Point object matches conditioned positions.
     *
     * @param point The submitted Point object.
     */
    private void printElements(Point point, OrkCamp orkCamp) {
        if (printOrksTest(point, orkCamp) == true) {
            printOrks(point, orkCamp);
        } else if (printCampNameTest(point, orkCamp) == true) {
            printCampName(point, orkCamp);
        } else if (printWallTest(point) == true) {
            printWall(point);
        } else {
            System.out.print(' ');
        }
    }

    /**
     * Displays Ork instances with their colour and badge.
     *
     * @param point The position that shall be checked.
     * @param orkCamp The instance of OrkCamp that shall be displayed.
     */
    private void printOrks(Point point, OrkCamp orkCamp) {
        for (OrkPack orkPack : orkCamp.getPacks()) {
            for (Ork ork : orkPack.getOrks()) {
                if (point.equals(ork.getPosition())) {
                    System.out.print(ork.getSort().getColour() + ork.getSort().getBadge() + Colours.ANSI_RESET);
                }
            }
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of Ork
     * instances.
     *
     * @param point The position that shall be checked.
     * @param orkCamp The instance of OrkCamp that shall be displayed.
     * @return Returns true if the submitted Point object equals to a Point
     * object marked as position of an Ork instance.
     */
    private boolean printOrksTest(Point point, OrkCamp orkCamp) {
        for (OrkPack orkPack : orkCamp.getPacks()) {
            for (Ork ork : orkPack.getOrks()) {
                if (point.equals(ork.getPosition())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Displays the name of the OrkCamp above.
     *
     * @param point The position that shall be checked.
     * @param orkCamp The instance of OrkCamp hat shall be displayed.
     */
    private void printCampName(Point point, OrkCamp orkCamp) {
        int pos = (maxWidth / 2) - (orkCamp.getName().length() / 2);
        for (int i = 0; i < orkCamp.getName().length(); i++) {
            if (point.x == pos && point.y == -1) {
                System.out.print(orkCamp.getName().toUpperCase().charAt(i));
            }
            pos++;
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of the
     * OrkCamp's name.
     *
     * @param point The position that shall be checked.
     * @param orkCamp The instance of OrkCamp that shall be displayed.
     * @return Returns true if the submitted Point object equals to a Point
     * object in the area marked for the OrkCamp's name
     */
    private boolean printCampNameTest(Point point, OrkCamp orkCamp) {
        int pos = (maxWidth / 2) - (orkCamp.getName().length() / 2);
        for (int i = -1; i < orkCamp.getName().length(); i++) {
            if (point.x == pos && point.y == -1) {
                return true;
            }
            pos++;
        }
        return false;
    }

    /**
     * Displays the surrounding fence.
     *
     * @param point The position that shall be checked
     */
    private void printWall(Point point) {
        if ((point.x == (minWidth) || point.x == (maxWidth)) && point.y >= 0) {
            System.out.print('|');
        } else if (point.y == (minHeight) || point.y == (maxHeight)) {
            System.out.print('-');
        }
    }

    /**
     * Checks if the submitted Point object equals to the position of the
     * surrounding fence.
     *
     * @param point The position that shall be checked
     * @return Returns true if the submitted Point object equals to a Point
     * object in the area marked for fence
     */
    private boolean printWallTest(Point point) {
        if ((point.x == (minWidth) || point.x == (maxWidth)) && point.y >= 0) {
            return true;
        } else if (point.y == (minHeight) || point.y == (maxHeight)) {
            return true;
        }
        return false;
    }

}
