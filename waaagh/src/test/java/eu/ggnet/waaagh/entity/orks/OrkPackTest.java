/*
*
* by Mirko Schulze
 */
package eu.ggnet.waaagh.entity.orks;

import static org.assertj.core.api.Assertions.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Administrator
 */
public class OrkPackTest {

    private Ork leader;
    private Ork orky;
    private Ork orker;
    private Ork orkay;
    private Ork punchingBag;
    private OrkPack attackerPack;
    private OrkPack defenderPack;

    public OrkPackTest() {
    }

    @Before
    public void setUp() {
        leader = new Ork("Ork", "Leader", 10, 20, 0);
        orky = new Ork("Ork", "Orky", 5, 10, 0);
        orker = new Ork("Ork", "Orker", 5, 10, 0);
        orkay = new Ork("Ork", "Orkay", 5, 10, 0);
        punchingBag = new Ork("Punching Bag", "Punching Bag", 0, 50, 0);
        attackerPack = new OrkPack(leader, orky, orker, orkay);
        defenderPack = new OrkPack(punchingBag);

    }

    /**
     * Test of getLeader method, of class OrkPack.
     */
    @Test
    public void testGetLeader() {
        assertThat(attackerPack.getLeader()).as("attackerPack leader should be Ork named leader")
                .isSameAs(leader);
    }

    /**
     * Test of attackOrkPack method, of class OrkPack.
     */
    @Test
    public void testAttackOrkPack() {
        attackerPack.attackOrkPack(defenderPack, false);
        assertThat(defenderPack.getOrks()).as("defenderPack should NOT be empty").isNotEmpty();
        assertThat(punchingBag.isAlive()).as("Ork named punchingBag should be alive").isTrue();
        //25 is half of punchingBags original health
        assertThat(punchingBag.getActHealth()).as("Ork named punchinBags health should be at 25 now").isEqualTo(25);
        attackerPack.attackOrkPack(defenderPack, false);
        assertThat(defenderPack.getOrks()).as("defenderPack should be empty").isEmpty();
        assertThat(punchingBag.isAlive()).as("Ork named punchingBag should be dead").isFalse();
    }

}
