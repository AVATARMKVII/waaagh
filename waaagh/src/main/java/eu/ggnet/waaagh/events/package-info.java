/*
 * Contains extensions or static classes to trigger special events.
 */
package eu.ggnet.waaagh.events;
