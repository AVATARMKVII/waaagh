package eu.ggnet.waaagh.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.tools.Utilities;
import eu.ggnet.waaagh.entity.orks.Ork;

/**
 * Contains static methods to stream messages "spoken" by an Ork.
 *
 * @author mirko.schulze
 */
public class OutputOrk {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputOrk() {

    }

    /**
     * Displays a message to indicate a succesful attack.
     *
     * @param ork The attacking Ork.
     */
    public static void hit(Ork ork) {
        String output;
        switch (Utilities.createRandomNumber(10)) {
            case 1:
                output = "'Den Arm brauchst'e doch eh nicht mehr!!'";
                break;
            case 2:
                output = "'Immer mitten in die Fresse rein!!'";
                break;
            case 3:
                output = "'Plitsch!Platsch!'";
                break;
            case 4:
                output = "'Flieg, kleines Köpfchen!Flieg!!'";
                break;
            case 5:
                output = "'Dakka!Dakka!Dakka!!'";
                break;
            case 6:
                output = "'Friss das!'";
                break;
            case 7:
                output = "'HACKEN!HACKEN!!'";
                break;
            case 8:
                output = "'Plündarn!Mördarn!'";
                break;
            case 9:
                output = "'Treffa!Haha!!'";
                break;
            default:
                output = "'Mosh'n!Mosh'n!MOSHEN!!'";
        }
        System.out.println(ork.getName() + ": " + ork.getSort().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a failing attack.
     *
     * @param ork The attacking Ork.
     */
    public static void miss(Ork ork) {
        String output;
        switch (Utilities.createRandomNumber(5)) {
            case 1:
                output = "'Drookkz! Daneben!!'";
                break;
            case 2:
                output = "'Nu' halt doch endlich still, Grot!'";
                break;
            case 3:
                output = "'Graah, schon wieder mit den Augen zu..'";
                break;
            case 4:
                output = "'Huch?!'";
                break;
            default:
                output = "'Verdammt!Abgerutscht!!'";
        }
        System.out.println(ork.getName() + ": " + ork.getSort().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        OutputNarrator.heMissed(ork);
        Utilities.sleepForASecond();
    }

    /**
     * Displays a message to indicate an instance of Ork is dying.
     *
     * @param ork The dying Ork.
     */
    public static void die(Ork ork) {
        String output;
        switch (Utilities.createRandomNumber(10)) {
            case 1:
                output = "'Uuurrkss...'";
                break;
            case 2:
                output = "'AAaargghh..'";
                break;
            case 3:
                output = "'Gllubb...Bllluugg..'";
                break;
            case 4:
                output = "'Nein, nicht du verdammter..Hund...'";
                break;
            case 5:
                output = "'Gork...ruft mich zu sich...'";
                break;
            case 6:
                output = "'Ich komm' zu dir....Mork'";
                break;
            case 7:
                output = "'Das wars...für mich...'";
                break;
            case 8:
                output = "'Röchel!Gurgel!Splurtz!'";
                break;
            case 9:
                output = "'Dafür...wirst du bezahlen..'";
                break;
            default:
                output = "'.....'";
        }
        System.out.println(ork.getName() + ": " + ork.getSort().getColour() + output + Colours.ANSI_RESET);
        OutputNarrator.heIsDead(ork);
        Utilities.sleepForASecond();
    }

    /**
     * Displays a message to indicate the victory in a fight scene.
     *
     * @param ork The victorious Ork.
     */
    public static void claimVictory(Ork ork) {
        System.out.println(ork.getName() + ": " + ork.getSort().getColour() + "'HAHA!!Stampft'se!Stampft'se bis nix mehr \u00fcbrich is!!'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

}
