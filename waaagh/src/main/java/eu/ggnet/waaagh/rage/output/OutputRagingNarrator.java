package eu.ggnet.waaagh.rage.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.entity.Narrator;
import eu.ggnet.waaagh.rage.entity.RagingOrkCamp;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages to indicate different stages of
 * the rage. Used for The Rage.
 *
 * @author mirko.schulze
 */
public class OutputRagingNarrator {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputRagingNarrator() {

    }

    /**
     * Streams a few messages to indicate the beginning of the story.
     *
     * @param ragingOrkCamp The participating RagingOrkCamp.
     */
    public static void introduction(RagingOrkCamp ragingOrkCamp) {
        System.out.println(Narrator.getNARRATOR() + "'Hinter den Bergen im Osten befindet sich ein kleines Dorf...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Doch wirst du hier keine tapferen Gallier finden...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Im Gegenteil, denn wir befinden uns vor dem Lager " + ragingOrkCamp.getName() + "...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Und hier herrscht der grausame Augenschluckerstamm...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Also lasst uns doch mal nachsehen, was dort vor sich geht...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
        System.out.println();
    }

    /**
     * Streams a few messages to indicate act I.
     */
    public static void actOne() {
        System.out.println(Narrator.getNARRATOR() + "'Nun, das sieht doch schon einmal gar nicht so lustig aus...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Ein Haufen stinkender Orks auf engem Raum, das kann nicht gut ausgehen...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Ihr letzter Raubzug liegt bereits lange zur\u00fcck...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Und sie sehnen sich nach Gewalt...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
        System.out.println();
    }

    /**
     * Streams a message to indicate a peaceful day in act I.
     */
    public static void peaceJoyPancakes() {
        System.out.println(Narrator.getNARRATOR() + "'Ein ruhiger Tag beim Augenschluckerstamm: Friede, Freude, Eierkuchen weit und breit...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Streams a few messages to indicate act II.
     */
    public static void actTwo() {
        System.out.println(Narrator.getNARRATOR() + "'Nun ist es passiert...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Die Orks k\u00f6nnen die Frotzeleien nicht mehr ertragen...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'...von den Visagen ganz zu schweigen...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Und machen sich bereit, die Sache zu regeln wie Orks...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
        System.out.println();
    }

    /**
     * Streams a few messages to indicate act III.
     *
     * @param ragingOrkCamp The participating RagingOrkCamp.
     */
    public static void actThree(RagingOrkCamp ragingOrkCamp) {
        if (ragingOrkCamp.getKommandoz().getRagingOrks().isEmpty()) {
            System.out.println(Narrator.getNARRATOR() + "'Wer h\u00e4tte das gedacht...'" + Colours.ANSI_RESET);
            Utilities.sleepForASecond();
            System.out.println(Narrator.getNARRATOR() + "'Die Anf\u00fchrer des Orkstammes wurden niedergeworfen....'" + Colours.ANSI_RESET);
            Utilities.sleepForASecond();
            System.out.println();
        } else {
            System.out.println(Narrator.getNARRATOR() + "'Wie im Rausch sind die Orks nicht imstande ihre Waffen zu senken...'" + Colours.ANSI_RESET);
            Utilities.sleepForASecond();
            System.out.println(Narrator.getNARRATOR() + "'Sie haben Blut gewittert...'" + Colours.ANSI_RESET);
            Utilities.sleepForASecond();
            System.out.println();
            System.out.println();
        }
    }

    /**
     * Displays a few messages to indicate act IV.
     */
    public static void actFour() {
        System.out.println(Narrator.getNARRATOR() + "'Die Schlacht ist geschlagen...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'...nun beginnt das blutige Fest...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
        System.out.println();
    }

    /**
     * Streams a few messages to indicate the end of the story.
     */
    public static void outroduction() {
        System.out.println();
        System.out.println();
        System.out.println(Narrator.getNARRATOR() + "'Nun, so langsam wird es wieder Zeit...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'Und gerade als die letzten Gliedma\u00dfen in die Feuer geworfen werden...'" + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println(Narrator.getNARRATOR() + "'...verabschieden wir uns von den Augenschluckern und ihrem fr\u00f6hlichen Treiben...'" + Colours.ANSI_RESET);
    }

}
