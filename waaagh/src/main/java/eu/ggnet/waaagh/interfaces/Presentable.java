package eu.ggnet.waaagh.interfaces;

/**
 * A testing functional interface to allow entities to display their attributes.
 *
 * @author mirko.schulze
 */
@FunctionalInterface
public interface Presentable {

    /**
     * Displays the most important attributes of an instance of Being.
     */
    abstract void present();

}
