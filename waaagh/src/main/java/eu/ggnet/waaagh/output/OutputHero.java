package eu.ggnet.waaagh.output;

import eu.ggnet.waaagh.distinctions.Colours;
import eu.ggnet.waaagh.entity.Hero;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * Contains static methods to stream messages "spoken" by a Hero.
 *
 * @author Administrator
 */
public class OutputHero {

    /**
     * Private constructor to prevent an instantiation.
     */
    private OutputHero() {

    }

    /**
     * Displays a message to indicate a succesful attack.
     *
     * @param hero The attacking Hero.
     */
    public static void hit(Hero hero) {
        String output;
        switch (Utilities.createRandomNumber(10)) {
            case 1:
                output = "'Ich bringe den Tod!'";
                break;
            case 2:
                output = "'Erzittert!'";
                break;
            case 3:
                output = "'Wehr dich doch!'";
                break;
            case 4:
                output = "'Damit hast du nicht gerechnet!'";
                break;
            default:
                output = "'Nimm das!!'";
        }
        System.out.println(hero.getName() + ": " + hero.getSort().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        System.out.println();
    }

    /**
     * Displays a message to indicate a failing attack.
     *
     * @param hero The attacking Hero.
     */
    public static void miss(Hero hero) {
        String output;
        switch (Utilities.createRandomNumber(3)) {
            case 1:
                output = "'Verdammt!'";
                break;
            case 2:
                output = "'Der ging daneben!'";
                break;
            default:
                output = "'Der Nächste trifft!'";
        }
        System.out.println(hero.getName() + ": " + hero.getSort().getColour() + output + Colours.ANSI_RESET);
        Utilities.sleepForASecond();
        OutputNarrator.heMissed(hero);
        Utilities.sleepForASecond();
    }

    /**
     * Displays a message to indicate an instance of Hero is dying.
     *
     * @param hero The dying Hero.
     */
    public static void die(Hero hero) {
        String tryIt;
        switch (Utilities.createRandomNumber(3)) {
            case 1:
                tryIt = "'Für das Imperium...'";
                break;
            case 2:
                tryIt = "'Für die Menschen...'";
                break;
            default:
                tryIt = "'Für Sigmar...'";
        }
        System.out.println(hero.getName() + ": " + hero.getSort().getColour() + tryIt + Colours.ANSI_RESET);
        OutputNarrator.heIsDead(hero);
        Utilities.sleepForASecond();
    }

}
