package eu.ggnet.waaagh.rage.entity;

import eu.ggnet.waaagh.rage.distinctions.OrkSquads;
import eu.ggnet.waaagh.tools.NamesGeneratorOrk;
import eu.ggnet.waaagh.tools.Utilities;

/**
 * RagingOrkLeader extends RagingOrk: extends RagingOrk by attributes
 * charisma(int), anger(int) and angry(boolean); offers methods getAngry and
 * calmDown; only fully initialized instances: used for The Rage.
 *
 * @author mirko.schulze
 */
public class RagingOrkLeader extends RagingOrk {

    private final int charisma;
    private int anger;
    private boolean angry;

    /**
     * Constructor to fully initialize an RagingOrkLeader as part of a
     * RagingOrkCamp.
     *
     * @param squad The visualization of this instance depends on sort.
     */
    public RagingOrkLeader(OrkSquads squad) {
        super();
        super.setName(NamesGeneratorOrk.createWaaaghBozzName());
        super.setSquad(squad);
        this.charisma = Utilities.createRandomAmountOfMembers();
    }

    public int getCharisma() {
        return charisma;
    }

    public int getAnger() {
        return anger;
    }

    public boolean isAngry() {
        return angry;
    }

    public void setAnger(int anger) {
        this.anger = anger;
    }

    public void setAngry(boolean angry) {
        this.angry = angry;
    }

    @Override
    public String toString() {
        return super.toString() + "RagingOrkLeader{" + "charisma=" + charisma + ", anger=" + anger + ", angry=" + angry + '}';
    }

    /**
     * Increases anger for this RagingOrkLeader by one. If five anger is reached
     * angry becomes true.
     */
    public void getAngry() {
        this.anger++;
        if (this.anger >= 5) {
            this.angry = true;
        }
    }

    /**
     * Sets anger to zero and angry to false for this RagingOrkLeader.
     */
    public void calmDown() {
        this.anger = 0;
        this.angry = false;
    }

}
